import "cypress-localstorage-commands";
import "cypress-file-upload";
const { generateString } = require("../../../../src/utils/random-string");

describe("QPCode Admin Categories tests", () => {
  let timeView;
  let timeInterval;
  let timeIntervalMedium;
  let LoginMock;
  let CategorieMock;
  let CategorieMockFail;
  let categorie;
  let UserMock;
  let idUser;
  let idCategory;

  let token;

  categorie = `${generateString().toUpperCase()}`;
  beforeEach(() => {
    timeView = 2000;
    timeIntervalMedium = 1500;
    timeInterval = 500;
    LoginMock = {
      email: `makeitreal@hotmail.com`,
      password: "123456",
    };
    UserMock = {
      names: `MakeItReal`,
      lastNames: "Bootcamp",
      phone: "999339593",
      address: "jr.7 de junio",
      description: "Prueba description personaje",
      email: `makeitreal@hotmail.com`,
      password: "123456",
      rol: "USER_ROLE",
    };
    CategorieMock = {
      nameCategory: `${categorie}`,
    };

    CategorieMockFail = {
      nameCategory: `SWIFT`,
    };
  });

  // ✅✔️✅
  it("Should render Admin Login page ", () => {
    cy.restoreLocalStorage();

    cy.visit("https://qpcode.netlify.app/admin");

    cy.get("#admin-login-email").type(LoginMock.email, { force: true });
    cy.wait(timeInterval);

    cy.get("#admin-login-password").type(LoginMock.password, { force: true });
    cy.wait(timeInterval);
    cy.get('[data-test-id="adminLogin-btn-ingresar"]').click({
      force: true,
    });

    cy.wait(timeView);
    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Success Login with registered user - Service", () => {
    cy.restoreLocalStorage();

    cy.request({
      method: "POST",
      url: "https://qpcode.herokuapp.com/api/auth/login",
      body: LoginMock,
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => {
      idUser = response.body.usuario.uid;
      token = response.body.token;

      expect(response.status).to.eq(200);
      expect(response.body.usuario.names).contains(`${UserMock.names}`);
      expect(response.body.usuario.lastNames).contains(`${UserMock.lastNames}`);
      expect(response.body.usuario.phone).contains(`${UserMock.phone}`);
      expect(response.body.usuario.email).contains(`${UserMock.email}`);
    });

    cy.wait(timeView);
    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Should render Admin Users page", () => {
    cy.restoreLocalStorage();
    cy.url().should("include", "/admin/users");

    cy.get("[data-test-id='sliderBar-categories-btn']").click({
      force: true,
    });

    cy.wait(timeView);

    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Should render Admin Categories page -  (View and Service) ", () => {
    cy.restoreLocalStorage();
    cy.url().should("include", "/admin/categories");

    cy.request("https://qpcode.herokuapp.com/api/categorias").then(
      (response) => {
        idCategory = response.body.categorias[0]._id;
        expect(response.status).to.eq(200);
        cy.wait(timeView);

        cy.get(
          `[data-test-id="admin-categories-nameCategory-${response.body.categorias[0]._id}"]`
        ).contains(response.body.categorias[0].nameCategory);
        cy.get(
          `[data-test-id="admin-categories-id-${response.body.categorias[0]._id}"]`
        ).contains(response.body.categorias[0]._id);
      }
    );

    cy.wait(timeView);

    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Should render Admin Categories page -  Modal Edit ", () => {
    cy.restoreLocalStorage();

    cy.get(`[data-test-id="menu-${idCategory}-btn"]`).click({
      force: true,
    });
    cy.wait(timeView);

    cy.get(`[data-test-id="menu-edit-btn"]`).click({
      force: true,
    });
    cy.wait(timeView);
    cy.saveLocalStorage();
  });

  // ❌🔴❌
  it("Error update Categorie data new (nameCategory duplicate) - View", () => {
    cy.restoreLocalStorage();

    cy.get("#admin-modal-nameCategory").clear({ force: true });
    cy.get("#admin-modal-nameCategory").type("Swift");

    cy.wait(timeIntervalMedium);

    cy.scrollTo("bottom");
    cy.wait(timeInterval);
    cy.get('[data-test-id="admin-modal-editBtn"]').click();
    cy.wait(timeIntervalMedium);
    cy.get(".Toastify__toast-body").contains(
      `La categoria ${CategorieMockFail.nameCategory}, ya existe`
    );
    cy.wait(timeView);
    cy.scrollTo(0, 0);
    cy.wait(timeIntervalMedium);
    cy.saveLocalStorage();
  });

  // ❌🔴❌
  it("Error update  Categorie data new (nameCategory duplicate) - Service", () => {
    cy.restoreLocalStorage();

    cy.request({
      method: "PUT",
      url: `https://qpcode.herokuapp.com/api/categorias/${idCategory}`,
      body: CategorieMockFail,
      failOnStatusCode: false,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    }).then((response) => {
      expect(response.status).to.eq(400);
      expect(response.body.msg).contains(
        `La categoria ${CategorieMockFail.nameCategory}, ya existe`
      );
    });
    cy.wait(timeView);
    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Success Create category - View ", () => {
    cy.restoreLocalStorage();

    cy.get(`[data-test-id="menu-new-btn"]`).click({
      force: true,
    });
    cy.wait(timeView);

    cy.get('[data-cy="categorie-imageFile-input"]').attachFile("Vue.png");
    cy.wait(timeIntervalMedium);
    cy.get("#admin-modal-nameCategory").clear({ force: true });
    cy.get("#admin-modal-nameCategory").type(CategorieMock.nameCategory);
    cy.wait(timeIntervalMedium);
    cy.get('[data-test-id="admin-modal-saveBtn"]').click();
    cy.wait(timeIntervalMedium);
    cy.get(".Toastify__toast-body").contains(
      `Categoría ${CategorieMock.nameCategory} Creado`
    );
    cy.wait(timeView);
    cy.scrollTo(0, 0);
    cy.wait(timeIntervalMedium);
    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Success validate Category create - Service", () => {
    cy.restoreLocalStorage();

    cy.request(
      `https://qpcode.herokuapp.com/api/buscar/categorias/${CategorieMock.nameCategory
        .toLowerCase()
        .replaceAll(" ", "-")}`
    ).then((response) => {
      idCategory = response.body.results[0]._id;
      expect(response.status).to.eq(200);
      expect(response.body.results[0].nameCategory).contains(
        `${CategorieMock.nameCategory}`
      );
    });
    cy.wait(timeView);
    cy.saveLocalStorage();
  });
  // ✅✔️✅
  it("Succes delete Category - View", () => {
    cy.restoreLocalStorage();

    cy.get(`[data-test-id="menu-${idCategory}-btn"]`).click({
      force: true,
    });
    cy.wait(timeView);

    cy.get(`[data-test-id="menu-delete-btn"]`).click({
      force: true,
    });
    cy.wait(timeView);
    cy.get(".Toastify__toast-body").contains(
      `Categoría ${CategorieMock.nameCategory} Eliminado`
    );
    cy.wait(timeView);

    cy.saveLocalStorage();
  });
});
