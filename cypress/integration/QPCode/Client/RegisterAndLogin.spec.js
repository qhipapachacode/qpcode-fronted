const { generateString } = require("../../../../src/utils/random-string");

describe("QPCode Register and Login tests", () => {
  let timeView;
  let timeInterval;
  let timeIntervalMedium;
  let LoginMock;
  let UserMock;
  let UserMockFail;
  let name;
  name = `${generateString()}`;

  beforeEach(() => {
    timeView = 2000;
    timeIntervalMedium = 1500;
    timeInterval = 500;
    LoginMock = {
      email: `${name}@hotmail.com`,
      password: "123456",
    };
    UserMock = {
      names: `${name}`,
      lastNames: "Vicuna Valle",
      phone: "999339593",
      address: "Jr 7 De Junio 265",
      email: `${name}@hotmail.com`,
      password: "123456",
    };
    UserMockFail = {
      names: `${name}`,
      lastNames: "Vicuna Valle",
      phone: "999339593",
      address: "Jr 7 De Junio 265",
      email: `jorge150896@hotmail.com`,
      password: "123456",
    };
  });

  // ✅✔️✅
  it("Should render Home page ", () => {
    cy.visit("https://qpcode.netlify.app/");
    cy.on("uncaught:exception", (err, runnable) => {
      return false;
    });
    cy.get('[data-test-id="home-header-title"]').contains("Qhipa-Co");
    cy.get('[data-test-id="home-header-btnText"]').contains("Ingresa Aqui");
    cy.wait(timeView);
    cy.get('[data-test-id="login-page-button"]').click();
    cy.wait(timeInterval);
  });

  // ✅✔️✅
  it("Should render Login page", () => {
    cy.url().should("include", "/login");
    cy.scrollTo(0, 0);
    cy.wait(timeView);
  });

  // ❌🔴❌

  it("Error Login with User not register - View", () => {
    cy.get("#login-email").type(LoginMock.email, { force: true });
    cy.scrollTo("top");
    cy.wait(timeInterval);

    cy.get("#login-password").type(LoginMock.password, { force: true });
    cy.scrollTo("top");

    cy.wait(timeInterval);

    cy.get('[data-test-id="login-btn-ingresar"]').click({
      force: true,
    });

    cy.get(".Toastify__toast-body").contains(
      "El correo electrónico es incorrecto."
    );
    cy.wait(timeView);
  });

  // ❌🔴❌

  it("Error Login with User not register - Service", () => {
    cy.request({
      method: "POST",
      url: "https://qpcode.herokuapp.com/api/auth/login",
      body: LoginMock,
      failOnStatusCode: false,
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => {
      expect(response.status).to.eq(400);
      expect(response.body.msg).contains(
        "El correo electrónico es incorrecto."
      );
    });
    cy.wait(timeView);
    cy.get('[data-test-id="login-page-button-register"]').click();
  });

  // ✅✔️✅
  it("Should render Register page", () => {
    cy.url().should("include", "/register");
    cy.scrollTo(0, 0);
    cy.wait(timeView);

    cy.get("#register-names").type(UserMock.names, { force: true });
    cy.scrollTo("top");
    cy.wait(timeInterval);

    cy.get("#register-lastNames").type(UserMock.lastNames, { force: true });
    cy.scrollTo("top");
    cy.wait(timeInterval);

    cy.get("#register-phone").type(UserMock.phone, { force: true });
    cy.scrollTo("top");
    cy.wait(timeInterval);

    cy.get("#register-address").type(UserMock.address, { force: true });
    cy.scrollTo("top");
    cy.wait(timeInterval);
  });

  // ❌🔴❌
  it("Error register User with email existing - View", () => {
    cy.scrollTo(0, 300);
    cy.wait(timeView);

    cy.get("#register-email").type(UserMockFail.email, { force: true });
    cy.wait(timeInterval);

    cy.get("#register-password").type(UserMock.password, { force: true });
    cy.wait(timeInterval);

    cy.get("#register-password2").type(UserMock.password, { force: true });
    cy.wait(timeInterval);

    cy.get('[data-test-id="register-btn-registrarme"]').click({
      force: true,
    });
    cy.wait(timeInterval);

    cy.get(".Toastify__toast-body").contains(
      "El email: jorge150896@hotmail.com, ya está registrado"
    );
    cy.wait(timeView);
  });

  // ❌🔴❌
  it("Error register User with email existing - Service", () => {
    cy.request({
      method: "POST",
      url: "https://qpcode.herokuapp.com/api/usuarios",
      body: UserMockFail,
      failOnStatusCode: false,
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => {
      expect(response.status).to.eq(400);
      expect(response.body.errors[0].msg).contains(
        `El email: ${UserMockFail.email}, ya está registrado`
      );
    });
    cy.wait(timeView);
  });

  // ✅✔️✅
  it("Success Register User - View", () => {
    cy.get("#register-email").clear();
    cy.get("#register-password").clear();
    cy.get("#register-password2").clear();

    cy.get("#register-email").type(UserMock.email, { force: true });
    cy.wait(timeInterval);

    cy.get("#register-password").type(UserMock.password, { force: true });
    cy.wait(timeInterval);

    cy.get("#register-password2").type(UserMock.password, { force: true });
    cy.wait(timeInterval);

    cy.get('[data-test-id="register-btn-registrarme"]').click({
      force: true,
    });
    cy.wait(timeInterval);
  });

  // ✅✔️✅
  it("Success validate registered user - Service", () => {
    cy.request(
      `https://qpcode.herokuapp.com/api/buscar/usuarios/${UserMock.names
        .toLowerCase()
        .replaceAll(" ", "-")}`
    ).then((response) => {
      expect(response.status).to.eq(200);
      expect(response.body.results[0].names).contains(`${UserMock.names}`);
      expect(response.body.results[0].lastNames).contains(
        `${UserMock.lastNames}`
      );
      expect(response.body.results[0].phone).contains(`${UserMock.phone}`);
      expect(response.body.results[0].email).contains(`${UserMock.email}`);
    });
    cy.wait(timeView);
  });

  // ✅✔️✅
  it("Should render Perfil page", () => {
    cy.url().should("include", "/perfil");
    cy.scrollTo(0, 0);
    cy.wait(timeView);
    cy.scrollTo("bottom");
    cy.wait(timeView);
    cy.scrollTo(0, 0);

    cy.get('[data-test-id="perfilSelect-page-button"]').click();
    cy.wait(timeIntervalMedium);
    cy.get('[data-test-id="logout-button"]').click();
    cy.wait(timeInterval);
  });
  // ✅✔️✅
  it("Should render Login page", () => {
    cy.url().should("include", "/login");
    cy.scrollTo(0, 0);
    cy.wait(timeView);
  });
  // ✅✔️✅
  it("Success Login with registered user - View", () => {
    cy.get("#login-email").type(LoginMock.email, { force: true });
    cy.scrollTo("top");
    cy.wait(timeInterval);

    cy.get("#login-password").type(LoginMock.password, { force: true });
    cy.scrollTo("top");

    cy.wait(timeInterval);

    cy.get('[data-test-id="login-btn-ingresar"]').click({
      force: true,
    });

    cy.wait(timeView);
  });

  // ✅✔️✅
  it("Success Login with registered user - Service", () => {
    cy.request({
      method: "POST",
      url: "https://qpcode.herokuapp.com/api/auth/login",
      body: LoginMock,
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => {
      expect(response.status).to.eq(200);
      expect(response.body.usuario.names).contains(`${UserMock.names}`);
      expect(response.body.usuario.lastNames).contains(`${UserMock.lastNames}`);
      expect(response.body.usuario.phone).contains(`${UserMock.phone}`);
      expect(response.body.usuario.email).contains(`${UserMock.email}`);
    });

    cy.wait(timeView);
  });
  // ✅✔️✅
  it("Should render Perfil page", () => {
    cy.url().should("include", "/perfil");
    cy.scrollTo(0, 0);
    cy.wait(timeView);
    cy.scrollTo("bottom");
    cy.wait(timeView);
    cy.scrollTo(0, 0);
    cy.wait(timeIntervalMedium);

    cy.get('[data-test-id="perfilSelect-page-button"]').click();
    cy.wait(timeIntervalMedium);
    cy.get('[data-test-id="logout-button"]').click();
    cy.wait(timeInterval);
  });
  // ✅✔️✅
  it("Should render Login page", () => {
    cy.url().should("include", "/login");
    cy.scrollTo(0, 0);
    cy.wait(timeView);
  });
});
