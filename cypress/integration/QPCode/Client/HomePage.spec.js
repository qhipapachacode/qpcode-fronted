// cy.visit("http://localhost:3000");

describe("QPCode Home tests", () => {
  let timeView;
  let timeInterval;
  let timeIntervalMedium;
  beforeEach(() => {
    timeView = 2000;
    timeIntervalMedium = 1500;
    timeInterval = 500;
  });

  // ✅✔️✅
  it("Should render Home page ", () => {
    cy.visit("https://qpcode.netlify.app/");
    cy.on("uncaught:exception", (err, runnable) => {
      return false;
    });
  });

  // ✅✔️✅
  it("Should render Home page - Header ", () => {
    cy.get('[data-test-id="home-header-title"]').contains("Qhipa-Co");
    cy.get('[data-test-id="home-header-btnText"]').contains("Ingresa Aqui");
    cy.wait(timeView);
  });

  // ✅✔️✅
  it("Should render Home page - Courses - (View and Service)", () => {
    cy.scrollTo(0, 700);
    cy.wait(timeIntervalMedium);
    cy.scrollTo(0, 1100);
    cy.wait(timeInterval);
    cy.scrollTo(0, 700);
    cy.wait(timeInterval);

    cy.request("https://qpcode.herokuapp.com/api/cursos").then((response) => {
      expect(response.status).to.eq(200);

      cy.get('[data-test-id="home-courses-cards-elements"]')
        .children()
        .should("to.have.length.of.at.most", response.body.cursos.length);

      cy.get('[data-test-id="home-courses-card-title"]').contains(
        response.body.cursos[0].nameCourse
      );
      cy.get('[data-test-id="home-courses-card-instructor"]').contains(
        response.body.cursos[0].instructor.lastNames
      );
    });
    cy.wait(timeView);
  });

  // ✅✔️✅
  it("Should redirect on Login Page - click in button Course withouth Sign In", () => {
    cy.get('[data-test-id="home-courses-card-redirect-button"]').click({
      force: true,
    });
    cy.url().should("include", "/login");
    cy.scrollTo(0, 0);
    cy.wait(timeView);
    cy.get('[data-test-id="home-page-button"]').click();
    cy.wait(timeInterval);
  });

  // ✅✔️✅
  it("Should render Home page - Notices", () => {
    cy.url().should("include", "/");
    cy.scrollTo(0, 1800);
    cy.wait(timeView);

    cy.get('[data-test-id="home-notices-cards-elements"]')
      .children()
      .should("to.have.length.of.at.most", 9);

    cy.scrollTo(0, 2300);
    cy.wait(timeView);

    cy.get('[data-test-id="home-notices-btnLink-elements"]')
      .children()
      .should("to.have.length.of.at.most", 12);
  });

  // ✅✔️✅
  it("Should render Home page - FAQ's - (View and Service)", () => {
    cy.scrollTo(0, 2950);
    cy.wait(timeView);
    cy.get('[data-test-id="home-questions-element-btn"]').click({
      force: true,
    });
    cy.wait(timeInterval);

    cy.request("https://qpcode.herokuapp.com/api/questions").then(
      (response) => {
        expect(response.status).to.eq(200);

        cy.get('[data-test-id="home-questions-elements"]')
          .children()
          .should("to.have.length.of.at.most", response.body.questions.length);

        cy.get('[data-test-id="home-questions-element-title"]').contains(
          response.body.questions[0].titleQuestion
        );
        cy.get('[data-test-id="home-questions-element-response"]').contains(
          response.body.questions[0].responseQuestion
        );
      }
    );
    cy.wait(timeView);
    cy.get('[data-test-id="home-questions-element-btn"]').click({
      force: true,
    });
    cy.wait(timeInterval);
    cy.scrollTo("top");
  });
});
