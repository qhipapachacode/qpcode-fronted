import "cypress-localstorage-commands";
import "cypress-file-upload";
const { generateString } = require("../../../../src/utils/random-string");

describe("QPCode Perfil and Course tests", () => {
  let timeView;
  let timeInterval;
  let timeIntervalMedium;
  let LoginMock;
  let UserMock;
  let UserMockMOdified;
  let UserMockFail;
  let name;
  let lastNames;
  let idUser;
  let token;
  let courseName;

  name = `${generateString()}`;
  lastNames = `${generateString()}`;

  beforeEach(() => {
    timeView = 2000;
    timeIntervalMedium = 1500;
    timeInterval = 500;
    LoginMock = {
      email: `makeitreal@hotmail.com`,
      password: "123456",
    };
    UserMock = {
      names: `MakeItReal`,
      lastNames: "Bootcamp",
      phone: "999339593",
      address: "jr.7 de junio",
      description: "Prueba description personaje",
      email: `makeitreal@hotmail.com`,
      password: "123456",
      rol: "USER_ROLE",
    };
    UserMockMOdified = {
      names: `${name}`,
      lastNames: `${lastNames}`,
      phone: "999339593",
      description: "Prueba description cambio de data a personaje",
      address: "Jr 7 De Junio 265",
      email: `makeitreal@hotmail.com`,
      password: "123456",
      rol: "USER_ROLE",
    };

    UserMockFail = {
      names: ``,
      lastNames: `${lastNames}`,
      phone: "999339593",
      description: "Prueba description cambio de data a personaje",
      address: "Jr 7 De Junio 265",
      email: `makeitreal@hotmail.com`,
      password: "123456",
      rol: "USER_ROLE",
    };
  });
  // ✅✔️✅
  it("Should render Home page ", () => {
    cy.visit("https://qpcode.netlify.app/");
    cy.on("uncaught:exception", (err, runnable) => {
      return false;
    });
    cy.get('[data-test-id="home-header-title"]').contains("Qhipa-Co");
    cy.get('[data-test-id="home-header-btnText"]').contains("Ingresa Aqui");
    cy.wait(timeView);
    cy.get('[data-test-id="login-page-button"]').click();
    cy.wait(timeInterval);
  });
  // ✅✔️✅
  it("Should render Login page", () => {
    cy.url().should("include", "/login");
    cy.scrollTo(0, 0);
    cy.wait(timeView);
  });

  // ✅✔️✅
  it("Success Login with registered user - View", () => {
    cy.restoreLocalStorage();

    cy.get("#login-email").type(LoginMock.email, { force: true });
    cy.scrollTo("top");
    cy.wait(timeInterval);

    cy.get("#login-password").type(LoginMock.password, { force: true });
    cy.scrollTo("top");

    cy.wait(timeInterval);

    cy.get('[data-test-id="login-btn-ingresar"]').click({
      force: true,
    });

    cy.wait(timeView);
    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Success Login with registered user - Service", () => {
    cy.restoreLocalStorage();

    cy.request({
      method: "POST",
      url: "https://qpcode.herokuapp.com/api/auth/login",
      body: LoginMock,
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => {
      idUser = response.body.usuario.uid;
      token = response.body.token;

      expect(response.status).to.eq(200);
      expect(response.body.usuario.names).contains(`${UserMock.names}`);
      expect(response.body.usuario.lastNames).contains(`${UserMock.lastNames}`);
      expect(response.body.usuario.phone).contains(`${UserMock.phone}`);
      expect(response.body.usuario.email).contains(`${UserMock.email}`);
    });

    cy.wait(timeView);
    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Should render Perfil page", () => {
    cy.restoreLocalStorage();

    cy.url().should("include", "/perfil");
    cy.scrollTo(0, 0);
    cy.wait(timeView);
    cy.scrollTo("bottom");
    cy.wait(timeView);
    cy.get('[data-test-id="perfil-btn-EditSave"]').click();
    cy.wait(timeIntervalMedium);
    cy.scrollTo(0, 0);

    cy.wait(timeIntervalMedium);

    cy.saveLocalStorage();
  });

  // ❌🔴❌
  it("Error update User data new (name Empty) - View", () => {
    cy.restoreLocalStorage();

    cy.get("#perfil-names").clear({ force: true });

    cy.wait(timeIntervalMedium);

    cy.scrollTo("top");
    cy.wait(timeInterval);

    cy.scrollTo("bottom");
    cy.wait(timeInterval);
    cy.get('[data-test-id="perfil-btn-EditSave"]').click();
    cy.wait(timeIntervalMedium);
    cy.get(".Toastify__toast-body").contains("El nombre es obligatorio");
    cy.wait(timeView);
    cy.scrollTo(0, 0);
    cy.wait(timeIntervalMedium);
    cy.scrollTo("bottom");
    cy.wait(timeView);
    cy.saveLocalStorage();
  });

  // ❌🔴❌
  it("Error update User data new (name Empty) - Service", () => {
    cy.restoreLocalStorage();

    cy.request({
      method: "PUT",
      url: `https://qpcode.herokuapp.com/api/usuarios/${idUser}`,
      body: UserMockFail,
      failOnStatusCode: false,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    }).then((response) => {
      expect(response.status).to.eq(400);
      expect(response.body.errors[0].msg).contains(`El nombre es obligatorio`);
    });
    cy.wait(timeView);
    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Success update User data new - View", () => {
    cy.restoreLocalStorage();
    cy.get('[data-test-id="perfil-btn-EditSave"]').click();
    cy.wait(timeIntervalMedium);
    cy.scrollTo(0, 0);
    cy.get('[data-cy="perfil-imageFile-input"]').attachFile("qpc.png");

    cy.get("#perfil-names").clear({ force: true });
    cy.get("#perfil-lastNames").clear({ force: true });
    cy.get("#perfil-description").clear({ force: true });
    cy.wait(timeIntervalMedium);

    cy.get("#perfil-names").type(UserMockMOdified.names, { force: true });
    cy.scrollTo("top");
    cy.wait(timeInterval);

    cy.get("#perfil-lastNames").type(UserMockMOdified.lastNames, {
      force: true,
    });
    cy.scrollTo("top");
    cy.scrollTo(0, 400);
    cy.wait(timeInterval);

    cy.get("#perfil-description").type(UserMockMOdified.description, {
      force: true,
    });
    cy.scrollTo("bottom");
    cy.wait(timeInterval);
    cy.get('[data-test-id="perfil-btn-EditSave"]').click();
    cy.wait(timeIntervalMedium);
    cy.get(".Toastify__toast-body").contains("Datos Actualizados");
    cy.wait(timeView);
    cy.scrollTo(0, 0);
    cy.wait(timeIntervalMedium);
    cy.get('[data-test-id="home-page-button"]').click();
    cy.wait(timeInterval);

    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Success validate Update User data new - Service", () => {
    cy.restoreLocalStorage();

    cy.request(
      `https://qpcode.herokuapp.com/api/buscar/usuarios/${UserMockMOdified.names
        .toLowerCase()
        .replaceAll(" ", "-")}`
    ).then((response) => {
      expect(response.status).to.eq(200);
      expect(response.body.results[0].names).contains(
        `${UserMockMOdified.names}`
      );
      expect(response.body.results[0].lastNames).contains(
        `${UserMockMOdified.lastNames}`
      );
      expect(response.body.results[0].description).contains(
        `${UserMockMOdified.description}`
      );
    });
    cy.wait(timeView);
    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Should render Home page", () => {
    cy.restoreLocalStorage();

    cy.url().should("include", "/");

    cy.on("uncaught:exception", (err, runnable) => {
      return false;
    });
    cy.scrollTo(0, 0);

    cy.get('[data-test-id="home-header-title"]').contains("Qhipa-Co");
    cy.get('[data-test-id="home-header-btnText"]').contains("Ingresa Aqui");
    cy.wait(timeView);
    cy.get('[data-test-id="perfilSelect-page-button"]').click();
    cy.wait(timeIntervalMedium);
    cy.get('[data-test-id="perfil-page-button"]').click();
    cy.wait(timeInterval);
    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Should render Perfil page", () => {
    cy.restoreLocalStorage();

    cy.url().should("include", "/perfil");
    cy.scrollTo(0, 0);
    cy.wait(timeView);
    cy.scrollTo("bottom");
    cy.wait(timeView);
    cy.get('[data-test-id="perfil-btn-EditSave"]').click();
    cy.wait(timeIntervalMedium);
    cy.scrollTo(0, 0);

    cy.wait(timeIntervalMedium);

    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Success update User original data - View", () => {
    cy.restoreLocalStorage();

    cy.get('[data-cy="perfil-imageFile-input"]').attachFile("MakeItReal.png");

    cy.get("#perfil-names").clear({ force: true });
    cy.get("#perfil-lastNames").clear({ force: true });
    cy.get("#perfil-description").clear({ force: true });
    cy.wait(timeIntervalMedium);

    cy.get("#perfil-names").type(UserMock.names, { force: true });
    cy.scrollTo("top");
    cy.wait(timeInterval);

    cy.get("#perfil-lastNames").type(UserMock.lastNames, {
      force: true,
    });
    cy.scrollTo("top");
    cy.scrollTo(0, 400);
    cy.wait(timeInterval);

    cy.get("#perfil-description").type(UserMock.description, {
      force: true,
    });
    cy.scrollTo("bottom");
    cy.wait(timeInterval);
    cy.get('[data-test-id="perfil-btn-EditSave"]').click();
    cy.wait(timeIntervalMedium);
    cy.get(".Toastify__toast-body").contains("Datos Actualizados");
    cy.wait(timeView);
    cy.scrollTo(0, 0);
    cy.wait(timeIntervalMedium);
    cy.get('[data-test-id="home-page-button"]').click();
    cy.wait(timeInterval);
    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Success validate Update User original data - Service", () => {
    cy.restoreLocalStorage();

    cy.request(
      `https://qpcode.herokuapp.com/api/buscar/usuarios/${UserMock.names
        .toLowerCase()
        .replaceAll(" ", "-")}`
    ).then((response) => {
      expect(response.status).to.eq(200);
      expect(response.body.results[0].names).contains(`${UserMock.names}`);
      expect(response.body.results[0].lastNames).contains(
        `${UserMock.lastNames}`
      );
      expect(response.body.results[0].description).contains(
        `${UserMock.description}`
      );
    });
    cy.wait(timeView);
    cy.saveLocalStorage();
  });

  // ✅✔️✅
  it("Should render Home page", () => {
    cy.url().should("include", "/");
    cy.scrollTo(0, 800);
    cy.wait(timeIntervalMedium);
    cy.scrollTo(0, 1200);
    cy.wait(timeInterval);
    cy.scrollTo(0, 800);
    cy.wait(timeInterval);
    cy.request("https://qpcode.herokuapp.com/api/cursos").then((response) => {
      courseName = response.body.cursos[0].nameCourse;

      expect(response.status).to.eq(200);
      cy.get('[data-test-id="home-courses-cards-elements"]')
        .children()
        .should("to.have.length.of.at.most", response.body.cursos.length);

      cy.get('[data-test-id="home-courses-card-title"]').contains(
        response.body.cursos[0].nameCourse
      );
      cy.get('[data-test-id="home-courses-card-instructor"]').contains(
        response.body.cursos[0].instructor.lastNames
      );
    });
    cy.wait(timeView);
  });

  // ✅✔️✅
  it("Should redirect on Course Page - click in button Course after login", () => {
    cy.get('[data-test-id="home-courses-card-redirect-button"]').click({
      force: true,
    });
    cy.url().should(
      "include",
      `/courses/${courseName.toLowerCase().replaceAll(" ", "-")}`
    );
    cy.scrollTo(0, 0);
    cy.wait(timeView);
    cy.scrollTo("bottom");
    cy.wait(timeView);
    cy.scrollTo(0, 0);
    cy.wait(timeView);
    cy.get('[data-test-id="home-page-button"]').click();
    cy.wait(timeInterval);
  });

  // ✅✔️✅
  it("Should render Home page", () => {
    cy.restoreLocalStorage();

    cy.url().should("include", "/");

    cy.on("uncaught:exception", (err, runnable) => {
      return false;
    });
    cy.scrollTo(0, 0);

    cy.get('[data-test-id="home-header-title"]').contains("Qhipa-Co");
    cy.get('[data-test-id="home-header-btnText"]').contains("Ingresa Aqui");
    cy.wait(timeView);

    cy.saveLocalStorage();
  });
});
