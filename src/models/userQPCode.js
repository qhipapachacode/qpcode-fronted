export const userQPCode = {
  lastNames: {
    type: String,
  },
  career: {
    type: String,
  },
  university: {
    type: String,
  },
  actualWork: {
    type: String,
  },
  nameCompany: {
    type: String,
  },
  linkCV: {
    type: String,
  },
  description: {
    type: String,
  },
  redLinkedin: {
    type: String,
  },
  redGitLab: {
    type: String,
  },
  redGitHub: {
    type: String,
  },
  redYoutube: {
    type: String,
  },
  redInsta: {
    type: String,
  },
  redFace: {
    type: String,
  },
  TecnologyOne: {
    type: String,
  },
  TecnologyTwo: {
    type: String,
  },
  TecnologyThree: {
    type: String,
  },
  activeMember: {
    type: Boolean,
  },
  activeInstructor: {
    type: Boolean,
  },
  img: {
    type: String,
  },
  phone: {
    type: String,
  },
  address: {
    type: String,
  },
  email: {
    type: String,
  },
};
