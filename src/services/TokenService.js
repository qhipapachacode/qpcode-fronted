export const getToken = () => {
  const token = `Bearer ${
    JSON.parse(localStorage.getItem("userQPCode")).userQPCode.token
  }`;
  return token;
};

export const getTokenAdmin = () => {
  const token = `Bearer ${
    JSON.parse(localStorage.getItem("userQPCode")).adminQPCode.token
  }`;
  return token;
};
// Tenerlo para uso futuro
export const setToken = (token) => {
  localStorage.setItem("token", token);
};
