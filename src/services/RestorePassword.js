import httpClient from "../utils/httpClient";

export const getRestorePassword = async (email) => {
  const body = { email: email };
  console.log(body);
  const data = await httpClient
    .post("/api/restorePass", body)
    .then((v) => v.data);
  return data;
};
