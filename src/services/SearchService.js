import httpClient from "../utils/httpClient";

export const getUsersSearch = async (user) => {
  const data = await httpClient
    .get(`/api/buscar/usuarios/${user}`)
    .then((v) => v.data);
  return data;
};

export const getCategoriesSearch = async (categoria) => {
  const data = await httpClient
    .get(`/api/buscar/categorias/${categoria}`)
    .then((v) => v.data);
  return data;
};

export const getCoursesSearch = async (curso) => {
  const data = await httpClient
    .get(`/api/buscar/cursos/${curso}`)
    .then((v) => v.data);
  return data;
};
