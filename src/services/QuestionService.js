import httpClient from "../utils/httpClient";
import { getTokenAdmin } from "./TokenService";

export const createQuestion = async (create) => {
  let config = {
    headers: {
      Authorization: getTokenAdmin(),
    },
  };

  try {
    const data = await httpClient
      .post("/api/questions", create, config)
      .then((v) => {
        return v.data;
      });
    return data;
  } catch (e) {
    return e.response.data;
  }
};

export const getQuestions = async () => {
  const data = await httpClient.get("/api/questions").then((v) => v.data);
  return data;
};

export const getQuestionbyId = async (question) => {
  const data = await httpClient
    .get(`/api/questions/${question}`)
    .then((v) => v.data);
  return data;
};

export const updateQuestionsbyId = async (update, question) => {
  let config = {
    headers: {
      Authorization: getTokenAdmin(),
    },
  };

  try {
    const data = await httpClient
      .put(`/api/questions/${question}`, update, config)
      .then((v) => {
        return v.data;
      });
    return data;
  } catch (e) {
    return e.response.data;
  }
};

export const deleteQuestionsbyId = async (question) => {
  let config = {
    headers: {
      Authorization: getTokenAdmin(),
    },
  };
  try {
    const data = await httpClient
      .delete(`/api/questions/${question}`, config)
      .then((v) => v.data);
    return data;
  } catch (e) {
    return e.response.data;
  }
};
