import httpClient from "../utils/httpClient";
import { getTokenAdmin } from "./TokenService";

export const registerUser = async (register) => {
  try {
    const data = await httpClient.post("/api/usuarios", register).then((v) => {
      return v.data;
    });
    return data;
  } catch (e) {
    return e.response.data.errors;
  }
};

export const getUsers = async () => {
  const data = await httpClient.get("/api/usuarios").then((v) => v.data);
  return data;
};

export const getUserbyId = async (user) => {
  console.log(user);
  const data = await httpClient
    .get(`/api/usuarios/${user}`)
    .then((v) => v.data);
  return data;
};

export const updateUserbyId = async (update, user) => {
  let config = {
    headers: {
      Authorization: getTokenAdmin(),
    },
  };

  try {
    const data = await httpClient
      .put(`/api/usuarios/${user}`, update, config)
      .then((v) => {
        return v.data;
      });
    return data;
  } catch (e) {
    return e.response.data;
  }
};

export const updateUserImagebyId = async (image, user) => {
  let config = {
    headers: {
      Authorization: getTokenAdmin(),
      "Content-Type": "multipart/form-data",
    },
  };

  try {
    const data = await httpClient
      .put(`/api/uploads/usuarios/${user}`, image, config)
      .then((v) => {
        return v.data;
      });
    return data;
  } catch (e) {
    return e.response.data;
  }
};

export const deleteUserbyId = async (usuario) => {
  let config = {
    headers: {
      Authorization: getTokenAdmin(),
    },
  };
  try {
    const data = await httpClient
      .delete(`/api/usuarios/${usuario}`, config)
      .then((v) => v.data);
    return data;
  } catch (e) {
    return e.response.data;
  }
};
