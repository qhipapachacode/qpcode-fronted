import httpClient from "../utils/httpClient";
import { getTokenAdmin } from "./TokenService";

export const createCourse = async (create) => {
  let config = {
    headers: {
      Authorization: getTokenAdmin(),
    },
  };

  try {
    const data = await httpClient
      .post("/api/cursos", create, config)
      .then((v) => {
        return v.data;
      });
    return data;
  } catch (e) {
    return e.response.data;
  }
};

export const getCourses = async () => {
  const data = await httpClient.get("/api/cursos").then((v) => v.data);
  return data;
};

export const getCoursesbyId = async (curso) => {
  console.log(curso);
  const data = await httpClient.get(`/api/cursos/${curso}`).then((v) => v.data);
  return data;
};

export const updatCoursesbyId = async (update, curso) => {
  let config = {
    headers: {
      Authorization: getTokenAdmin(),
    },
  };

  try {
    const data = await httpClient
      .put(`/api/cursos/${curso}`, update, config)
      .then((v) => {
        return v.data;
      });
    return data;
  } catch (e) {
    return e.response.data;
  }
};

export const updateCoursesImagebyId = async (image, curso) => {
  let config = {
    headers: {
      Authorization: getTokenAdmin(),
      "Content-Type": "multipart/form-data",
    },
  };

  try {
    const data = await httpClient
      .put(`/api/uploads/cursos/${curso}`, image, config)
      .then((v) => {
        return v.data;
      });
    return data;
  } catch (e) {
    return e.response.data;
  }
};

export const deleteCoursesbyId = async (curso) => {
  let config = {
    headers: {
      Authorization: getTokenAdmin(),
    },
  };
  try {
    const data = await httpClient
      .delete(`/api/cursos/${curso}`, config)
      .then((v) => v.data);
    return data;
  } catch (e) {
    return e.response.data;
  }
};
