import httpClient from "../utils/httpClient";
import { getTokenAdmin } from "./TokenService";

export const createCategory = async (create) => {
  let config = {
    headers: {
      Authorization: getTokenAdmin(),
    },
  };

  try {
    const data = await httpClient
      .post("/api/categorias", create, config)
      .then((v) => {
        return v.data;
      });
    return data;
  } catch (e) {
    return e.response.data;
  }
};

export const getCategories = async () => {
  const data = await httpClient.get("/api/categorias").then((v) => v.data);
  return data;
};

export const getCategoriesbyId = async (categoria) => {
  const data = await httpClient
    .get(`/api/categorias/${categoria}`)
    .then((v) => v.data);
  return data;
};

export const updateCategoriesbyId = async (update, categoria) => {
  let config = {
    headers: {
      Authorization: getTokenAdmin(),
    },
  };

  try {
    const data = await httpClient
      .put(`/api/categorias/${categoria}`, update, config)
      .then((v) => {
        return v.data;
      });
    return data;
  } catch (e) {
    return e.response.data;
  }
};

export const updateCategoriesImagebyId = async (image, categoria) => {
  let config = {
    headers: {
      Authorization: getTokenAdmin(),
      "Content-Type": "multipart/form-data",
    },
  };

  try {
    const data = await httpClient
      .put(`/api/uploads/categorias/${categoria}`, image, config)
      .then((v) => {
        return v.data;
      });
    return data;
  } catch (e) {
    return e.response.data;
  }
};

export const deleteCategoriesbyId = async (categoria) => {
  let config = {
    headers: {
      Authorization: getTokenAdmin(),
    },
  };
  try {
    const data = await httpClient
      .delete(`/api/categorias/${categoria}`, config)
      .then((v) => v.data);
    return data;
  } catch (e) {
    return e.response.data;
  }
};
