import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Modal, Form } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";
import { AuthContext } from "../../../auth/authContext";
import AddAPhotoIcon from "@material-ui/icons/AddAPhoto";

import { types } from "../../../types/types";
import avatar from "../../../assets/img/iconQPC.svg";

import {
  createCategory,
  updateCategoriesbyId,
  updateCategoriesImagebyId,
} from "../../../services/CategoryService.js";

import "./AdminModalCategoryPage.scss";

let isToken = false;

const AdminModalCategoryPage = ({
  show,
  handleClose,
  handleReload,
  type,
  data,
}) => {
  const navigate = useNavigate();
  const { user, dispatch } = useContext(AuthContext);
  const [form, setForm] = useState({ nameCategory: `` });
  const [selectedFile, setSelectedFile] = useState();
  const [imageFile, setImageFile] = useState(avatar);

  useEffect(() => {
    if (show == true) {
      setSelectedFile(undefined);
      {
        data.nameCategory == undefined
          ? setForm({ nameCategory: `` })
          : setForm({ nameCategory: `${data.nameCategory}` });
      }
    }
  }, [show]);

  const createCategoryFromApi = () => {
    const formData = new FormData();
    formData.append("archivo", selectedFile);

    createCategory(form).then((data) => {
      if (data.status == 1) {
        updateCategoriesImageFromApi(formData, data.categoria._id);

        toast.success(`Categoría ${data.categoria.nameCategory} Creado`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
      } else if (data.msg != undefined) {
        setSelectedFile(undefined);

        if (data.msg === "Token no válido") {
          isToken = true;

          dispatch({ type: types.logoutAdmin });
          navigate("/admin", {
            replace: true,
          });
        } else {
          toast.error(`${data.msg}`, {
            position: toast.POSITION.BOTTOM_LEFT,
            autoClose: 3000,
            pauseOnHover: false,
          });
        }
      } else {
        setSelectedFile(undefined);

        toast.error(`${data.errors[0].msg}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
      }
    });
  };

  const updateCategoriesImageFromApi = (image, idCategory) => {
    updateCategoriesImagebyId(image, idCategory).then((data) => {
      if (data.status === 1) {
        toast.success(`Imagen Categoría Actualizado`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
        handleReload();
      } else {
        handleReload();

        if (data.msg === "Token no válido") {
          isToken = true;

          dispatch({ type: types.logoutAdmin });

          navigate("/admin", {
            replace: true,
          });
        } else {
          if (data.msg === "No hay archivos que subir - validarArchivoSubir") {
            toast.success(`No se cargo o modifico Imagen`, {
              position: toast.POSITION.BOTTOM_LEFT,
              pauseOnHover: false,
            });
          } else {
            toast.error(`${data.msg}`, {
              position: toast.POSITION.BOTTOM_LEFT,
              pauseOnHover: false,
            });
          }
        }
      }
    });
  };

  const updateCategoryFromApi = () => {
    const formData = new FormData();
    formData.append("archivo", selectedFile);

    updateCategoriesbyId(form, data._id).then((data) => {
      if (data.status == 1) {
        updateCategoriesImageFromApi(formData, data.categoria._id);

        toast.success(`Dato Actualizado`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
      } else if (data.msg != undefined) {
        setSelectedFile(undefined);

        if (data.msg === "Token no válido") {
          isToken = true;

          dispatch({ type: types.logoutAdmin });
          navigate("/admin", {
            replace: true,
          });
        } else {
          toast.error(`${data.msg}`, {
            position: toast.POSITION.BOTTOM_LEFT,
            autoClose: 3000,
            pauseOnHover: false,
          });
        }
      } else {
        setSelectedFile(undefined);

        toast.error(`${data.errors[0].msg}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
      }
    });
  };

  const handleForm = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const handleSave = () => {
    createCategoryFromApi();
    handleClose();
  };
  const handleEdit = () => {
    updateCategoryFromApi();
    handleClose();
  };
  const imageHandler = (e) => {
    setSelectedFile(e.target.files[0]);

    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        setImageFile(reader.result);
      }
    };
    if (e.target.files[0]) {
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  return (
    <>
      <ToastContainer autoClose={1500} />

      <Modal show={show} onHide={handleClose} className="AdminModalCategory">
        <Modal.Header closeButton>
          <Modal.Title className=" AdminModalCategory__form__datos__title">
            {type == "CREAR" ? "Crear " : type == "EDITAR" ? "Editar " : "Ver "}
            Categoría
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            {type == "VER" ? (
              ""
            ) : (
              <div className=" AdminModalCategory__form__datos__icons">
                <label
                  className={
                    "AdminModalCategory__form__datos__icons__icon camera"
                  }
                  for="fileupload"
                >
                  <AddAPhotoIcon />
                </label>

                <input
                  type="file"
                  id="fileupload"
                  onChange={imageHandler}
                  data-cy="categorie-imageFile-input"
                  style={{ visibility: "hidden", display: "none" }}
                />
              </div>
            )}
            <div className="AdminModalCategory__form__datos__image">
              <img
                src={
                  (data.img == undefined || data.img == "") &&
                  selectedFile === undefined
                    ? avatar
                    : selectedFile === undefined
                    ? data.img
                    : imageFile
                }
                alt=""
                style={{ width: "100px" }}
              />
            </div>

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Nombre de Categoría*</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ingresa un nombre"
                autoFocus
                onChange={handleForm}
                name="nameCategory"
                disabled={type == "VER" ? true : false}
                value={`${form.nameCategory}`}
                id="admin-modal-nameCategory"
              />
            </Form.Group>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            ></Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <button className="button__colorGray" onClick={handleClose}>
            Cerrar
          </button>
          {type == "VER" ? (
            ""
          ) : type == "CREAR" ? (
            <button
              className="button"
              onClick={handleSave}
              data-test-id="admin-modal-saveBtn"
              disabled={form.nameCategory.length > 0 ? false : true}
            >
              Guardar
            </button>
          ) : (
            <button
              className="button"
              onClick={handleEdit}
              data-test-id="admin-modal-editBtn"
              disabled={form.nameCategory.length > 0 ? false : true}
            >
              Guardar
            </button>
          )}
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default AdminModalCategoryPage;
