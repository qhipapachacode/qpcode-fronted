import React from "react";
// component
import Iconify from "../../components/Iconify";

// ----------------------------------------------------------------------

const getIcon = (name) => <Iconify icon={name} width={22} height={22} />;

const sidebarConfig = [
  {
    title: "users",
    path: "/admin/users",
    icon: getIcon("eva:people-fill"),
  },
  {
    title: "categories",
    path: "/admin/categories",
    icon: getIcon("bxs:category"),
  },
  {
    title: "courses",
    path: "/admin/courses",
    icon: getIcon("fluent:hat-graduation-24-filled"),
  },
  {
    title: "questions",
    path: "/admin/questions",
    icon: getIcon("eva:file-text-fill"),
  },
];

export default sidebarConfig;
