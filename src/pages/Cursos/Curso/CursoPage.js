import React, { useEffect, useState } from "react";
import { Fade } from "react-reveal";
import { Link, useParams } from "react-router-dom";
import GridLoader from "react-spinners/GridLoader";
import { Col, Card, Button, Accordion } from "react-bootstrap";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import GithubIcon from "@material-ui/icons/GitHub";
import YoutubeIcon from "@material-ui/icons/YouTube";
import { FaGitlab } from "react-icons/fa";
import { AiFillLinkedin } from "react-icons/ai";
import "./CursoPage.scss";
import { getCoursesSearch } from "../../../services/SearchService";
import { getUserbyId } from "../../../services/UserService";
import SecondaryButton from "../../../components/SecondaryButoon/SecondaryButton";
const CursoPage = () => {
  const [loadingPage, setLoadingPage] = useState(true);

  const { id } = useParams();

  const [curso, setCurso] = useState("");
  const [instructor, setInstructor] = useState("");

  useEffect(() => {
    getCoursesSearchFromApi();
  }, []);

  const getCoursesSearchFromApi = () => {
    setLoadingPage(true);
    getCoursesSearch(id).then((data) => {
      if (data.results.length > 0) {
        setCurso(data.results[0]);
        getUserFromApi(data.results[0].instructor._id);
      }
    });
  };
  const getUserFromApi = async (idUser) => {
    getUserbyId(idUser).then((data) => {
      setInstructor(data);
      setLoadingPage(false);
    });
  };
  return (
    <>
      <div className="cursoPage">
        {loadingPage ? (
          <div className="cursoPage__content__loading">
            <GridLoader color={"rgb(80, 43, 207)"} size={30} />
          </div>
        ) : (
          <div className="cursoPage__content">
            <div
              className="row"
              style={{ display: "flex", justifyContent: "space-between" }}
            >
              <Fade>
                <div className="cursoPage__content__portada">
                  <img
                    src={curso.img}
                    alt=""
                    className="cursoPage__content__portada__img"
                  />
                </div>
              </Fade>
            </div>
            <div
              className="row"
              style={{ display: "flex", justifyContent: "space-between" }}
            >
              <Fade>
                <div className="col col-12 col-md-7 cursoPage__content__info ">
                  <div className="cursoPage__content__info__title mb-2 ">
                    {curso.nameCourse}
                  </div>

                  <div className="cursoPage__content__info__subtitle ">
                    <p> {curso.description}</p>
                  </div>
                  <div className="cursoPage__content__info__button ">
                    <SecondaryButton
                      name={"Ver Lista de Videos"}
                      link={curso.LinkYoutube}
                    />
                  </div>
                </div>
              </Fade>
              <Fade>
                <div className="col col-12 col-md-4 cursoPage__content__teacher ">
                  <div className="cursoPage__content__teacher__title mb-2 ">
                    Instructor:
                  </div>
                  <div className="AdminModalCategory__form__datos__image">
                    <img
                      src={instructor.img}
                      alt=""
                      style={{ width: "100px" }}
                    />
                  </div>
                  <div className="cursoPage__content__teacher__name ">
                    {instructor.names} {instructor.lastNames}
                  </div>
                  <div className="cursoPage__content__teacher__work ">
                    {instructor.actualWork == ""
                      ? ""
                      : `(${instructor.actualWork})`}
                  </div>
                  <div className="cursoPage__content__teacher__icons">
                    {instructor.redGitLab != "" && (
                      <a
                        target="_blank"
                        href={instructor.redGitLab}
                        rel="noreferrer"
                        className="icon i-gitlab"
                      >
                        <FaGitlab style={{ width: "15" }} />
                      </a>
                    )}
                    {instructor.redLinkedin != "" && (
                      <a
                        target="_blank"
                        href={instructor.redLinkedin}
                        rel="noreferrer"
                        className="icon i-linkedin"
                      >
                        <AiFillLinkedin style={{ width: "15" }} />
                      </a>
                    )}
                    {instructor.redGitHub != "" && (
                      <a
                        target="_blank"
                        href={instructor.redGitHub}
                        rel="noreferrer"
                        className="icon i-github"
                      >
                        <GithubIcon style={{ width: "15" }} />
                      </a>
                    )}
                    {instructor.redYoutube != "" && (
                      <a
                        target="_blank"
                        href={instructor.redYoutube}
                        rel="noreferrer"
                        className="icon i-youtube"
                      >
                        <YoutubeIcon style={{ width: "15" }} />
                      </a>
                    )}
                    {instructor.redInsta != "" && (
                      <a
                        target="_blank"
                        href={instructor.redInsta}
                        rel="noreferrer"
                        className="icon i-instagram"
                      >
                        <InstagramIcon style={{ width: "15" }} />
                      </a>
                    )}
                    {instructor.redFace != "" && (
                      <a
                        target="_blank"
                        href={instructor.redFace}
                        rel="noreferrer"
                        className="icon i-facebook"
                      >
                        <FacebookIcon style={{ width: "15" }} />
                      </a>
                    )}
                  </div>
                </div>
              </Fade>
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default CursoPage;
