import React, { useState } from "react";
import { InnerLayout, OuterLayout } from "../../styles/Layouts";
import cursos from "../../data/cursos";
import MenuCard from "../../components/MenuCard/MenuCard";
import Button from "../../components/Button";
import HeaderHome from "../../components/HeaderHome/HeaderHome";
import { Fade, Flip } from "react-reveal";
import Title from "../../components/Title/Title";

const allButtons = ["All", ...new Set(cursos.map((item) => item.category))];

function CursosPage() {
  const [menuItem, setMenuItems] = useState(cursos);
  const [button, setButtons] = useState(allButtons);

  const filter = (button) => {
    if (button === "All") {
      setMenuItems(cursos);
      return;
    }

    const filteredData = cursos.filter((item) => item.category === button);
    setMenuItems(filteredData);
  };
  return (
    <>
      <HeaderHome />

      <OuterLayout>
        <Fade left>
          <Title title={"Cursos"} span={"Cursos"} />
        </Fade>
        <InnerLayout>
          <Fade right>
            <Button filter={filter} button={button} />
          </Fade>
          <Fade bottom>
            <MenuCard menuItem={menuItem} />
          </Fade>
        </InnerLayout>
      </OuterLayout>
    </>
  );
}

export default CursosPage;
