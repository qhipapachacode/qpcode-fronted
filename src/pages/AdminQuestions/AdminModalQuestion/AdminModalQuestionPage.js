import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Modal, Form } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";
import { AuthContext } from "../../../auth/authContext";
import AddAPhotoIcon from "@material-ui/icons/AddAPhoto";

import { types } from "../../../types/types";
import avatar from "../../../assets/img/iconQPC.svg";

import {
  createQuestion,
  updateQuestionsbyId,
} from "../../../services/QuestionService.js";

import "./AdminModalQuestionPage.scss";

let isToken = false;

const AdminModalQuestionPage = ({
  show,
  handleClose,
  handleReload,
  type,
  data,
}) => {
  const navigate = useNavigate();
  const { user, dispatch } = useContext(AuthContext);
  const [form, setForm] = useState({ titleQuestion: `` });

  useEffect(() => {
    if (show == true) {
      {
        if (isObjEmpty(data) == true) {
          setForm({ titleQuestion: ``, responseQuestion: `` });
        } else {
          setForm({
            titleQuestion: `${data.titleQuestion}`,
            responseQuestion: `${data.responseQuestion}`,
          });
        }
      }
    }
  }, [show]);

  function isObjEmpty(obj) {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  }

  const createQuestionFromApi = () => {
    console.log("data:");

    console.log(form);
    createQuestion(form).then((data) => {
      if (data.status == 1) {
        toast.success(`Question ${data.question.titleQuestion} Creado`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
      } else if (data.msg != undefined) {
        if (data.msg === "Token no válido") {
          isToken = true;

          dispatch({ type: types.logoutAdmin });
          navigate("/admin", {
            replace: true,
          });
        } else {
          toast.error(`${data.msg}`, {
            position: toast.POSITION.BOTTOM_LEFT,
            autoClose: 3000,
            pauseOnHover: false,
          });
        }
      } else {
        toast.error(`${data.errors[0].msg}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
      }
      handleReload();
    });
  };

  const updateQuestionFromApi = () => {
    updateQuestionsbyId(form, data._id).then((data) => {
      if (data.status == 1) {
        toast.success(`Dato Actualizado`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
      } else if (data.msg != undefined) {
        if (data.msg === "Token no válido") {
          isToken = true;

          dispatch({ type: types.logoutAdmin });
          navigate("/admin", {
            replace: true,
          });
        } else {
          toast.error(`${data.msg}`, {
            position: toast.POSITION.BOTTOM_LEFT,
            autoClose: 3000,
            pauseOnHover: false,
          });
        }
      } else {
        toast.error(`${data.errors[0].msg}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
      }
      handleReload();
    });
  };

  const handleForm = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const handleSave = () => {
    createQuestionFromApi();
    handleClose();
  };
  const handleEdit = () => {
    updateQuestionFromApi();
    handleClose();
  };

  return (
    <>
      <ToastContainer autoClose={1500} />

      <Modal show={show} onHide={handleClose} className="AdminModalQuestion">
        <Modal.Header closeButton>
          <Modal.Title className="AdminModalQuestion__form__datos__title">
            {type == "CREAR" ? "Crear " : type == "EDITAR" ? "Editar " : "Ver "}
            Pregunta
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Pregunta*</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ingresa un nombre"
                autoFocus
                onChange={handleForm}
                name="titleQuestion"
                disabled={type == "VER" ? true : false}
                value={`${form.titleQuestion}`}
              />
            </Form.Group>
            <Form.Label>Respuesta</Form.Label>
            <Form.Control
              as="textarea"
              rows={3}
              onChange={handleForm}
              name="responseQuestion"
              disabled={type == "VER" ? true : false}
              value={`${form.responseQuestion}`}
            />
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            ></Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <button className="button__colorGray" onClick={handleClose}>
            Cerrar
          </button>
          {type == "VER" ? (
            ""
          ) : type == "CREAR" ? (
            <button
              className="button"
              onClick={handleSave}
              disabled={form.titleQuestion.length > 0 ? false : true}
            >
              Guardar
            </button>
          ) : (
            <button
              className="button"
              onClick={handleEdit}
              disabled={form.titleQuestion.length > 0 ? false : true}
            >
              Guardar
            </button>
          )}
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default AdminModalQuestionPage;
