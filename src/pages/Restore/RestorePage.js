import { css } from "@emotion/react";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import TextField from "@mui/material/TextField";
import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import RingLoader from "react-spinners/RingLoader";
import { toast, ToastContainer } from "react-toastify";
import ParticleTwo from "../../components/ParticleTwo/ParticleTwo";
import { Fade } from "react-reveal";

import "react-toastify/dist/ReactToastify.css";
import { getRestorePassword } from "../../services/RestorePassword";
import "./RestorePage.scss";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: "#fff";
`;

const theme = createTheme({
  palette: {
    primary: {
      main: "rgb(80, 43, 207)",
    },
  },
});

const RestorePage = () => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState("");

  const restorePasswordFromApi = () => {
    setLoading(true);

    getRestorePassword(email).then((data) => {
      if (data.status === 1) {
        toast.success(`${data.msg}, redireccionando...`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
          onClose: () => {
            setLoading(false);

            navigate("/login", {
              replace: true,
            });
          },
        });
      } else {
        setLoading(false);
        toast.error(`${data.msg}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
      }
    });
  };

  const handleForm = (e) => {
    setEmail(e.target.value);
  };

  return (
    <>
      <div className="restore">
        <ToastContainer autoClose={1500} />
        <Fade>
          <form
            className="restore__form"
            onSubmit={(e) => {
              e.preventDefault();
              restorePasswordFromApi();
            }}
          >
            <Link to="/login">
              <div className="restore__form__back mb-5">
                <ArrowBackIosIcon />
                Regresar
              </div>
            </Link>

            <div className="restore__form__title mb-2">
              ¿Olvidaste tu contraseña?
            </div>

            <div className="restore__form__subtitle mb-5">
              Por favor ingrese su correo electrónico.
            </div>
            <ThemeProvider theme={theme}>
              <div className="row  restore__form__datos">
                <div className="col col-12 ">
                  <TextField
                    id="outlined-basic"
                    label="Email *"
                    variant="outlined"
                    name="email"
                    value={email}
                    fullWidth
                    type="email"
                    className="mb-4"
                    placeholder="Ingresa tu Correo Electrónico"
                    focused
                    disabled={loading}
                    onChange={handleForm}
                  />
                </div>
              </div>
              <div className="row restore__form__datos">
                {}
                <div className=" restore__form__datos__button mt-3">
                  {email === "" ? (
                    <button type="button" className="button" disabled>
                      Restaurar Contraseña
                    </button>
                  ) : (
                    <button
                      className="button restore__form__textButton"
                      disabled={loading}
                    >
                      {loading ? (
                        <RingLoader color={"#fff"} css={override} size={40} />
                      ) : (
                        "Restaurar Contraseña"
                      )}
                    </button>
                  )}
                </div>
              </div>
            </ThemeProvider>
          </form>
        </Fade>
      </div>
    </>
  );
};

export default RestorePage;
