import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Modal, Form, Dropdown } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";
import { AuthContext } from "../../../auth/authContext";
import AddAPhotoIcon from "@material-ui/icons/AddAPhoto";

import { types } from "../../../types/types";
import avatar from "../../../assets/img/iconQPC2.svg";

import {
  createCourse,
  updatCoursesbyId,
  updateCoursesImagebyId,
} from "../../../services/CourseService.js";
import { getCategories } from "../../../services/CategoryService.js";
import { getUsers } from "../../../services/UserAdminService";

import "./AdminModalCoursesPage.scss";

let isToken = false;

const AdminModalCoursesPage = ({
  show,
  handleClose,
  handleReload,
  type,
  data,
}) => {
  const navigate = useNavigate();
  const { user, dispatch } = useContext(AuthContext);
  const [form, setForm] = useState({ nameCourse: ``, category: `` });
  const [selectedFile, setSelectedFile] = useState();
  const [imageFile, setImageFile] = useState(avatar);
  const [categories, setCategories] = useState([]);
  const [categorieSelect, setCategorieSelect] = useState("");
  const [usersSelect, setUsersSelect] = useState([]);
  const [isSwitchOn, setIsSwitchOn] = useState(false);
  const [teachers, setTeachers] = useState([]);

  useEffect(() => {
    if (show == true) {
      setSelectedFile(undefined);
      {
        if (isObjEmpty(data) == true) {
          setForm({
            nameCourse: ``,
            category: ``,
            instructor: ``,
            description: ``,
            LinkYoutube: ``,
          });
          setCategorieSelect("");
          setUsersSelect("");
          setIsSwitchOn(false);
        } else {
          setForm({
            nameCourse: `${data.nameCourse}`,
            category: `${data.category._id}`,
            instructor: `${data.instructor._id}`,
            description: `${data.description}`,
            LinkYoutube: `${data.LinkYoutube}`,
          });
          setCategorieSelect(data.category.nameCategory);
          setUsersSelect(data.instructor.lastNames);
          setIsSwitchOn(data.outstanding);
        }
      }
      if (type == "CREAR" || type == "EDITAR") {
        getCategoriesFromApi();
        getUsersFromApi();
      }
    }
  }, [show]);

  const createCourseFromApi = () => {
    const formData = new FormData();
    formData.append("archivo", selectedFile);

    createCourse(form).then((data) => {
      if (data.status == 1) {
        updateCoursesImageFromApi(formData, data.curso._id);

        toast.success(`Curso ${data.curso.nameCourse} Creado`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
      } else if (data.msg != undefined) {
        setSelectedFile(undefined);

        if (data.msg === "Token no válido") {
          isToken = true;

          dispatch({ type: types.logoutAdmin });
          navigate("/admin", {
            replace: true,
          });
        } else {
          toast.error(`${data.msg}`, {
            position: toast.POSITION.BOTTOM_LEFT,
            autoClose: 3000,
            pauseOnHover: false,
          });
        }
      } else {
        setSelectedFile(undefined);

        toast.error(`${data.errors[0].msg}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
      }
    });
  };

  const updateCoursesImageFromApi = (image, idCourse) => {
    updateCoursesImagebyId(image, idCourse).then((data) => {
      if (data.status === 1) {
        toast.success(`Imagen Curso Actualizado`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
        handleReload();
      } else {
        handleReload();

        if (data.msg === "Token no válido") {
          isToken = true;

          dispatch({ type: types.logoutAdmin });

          navigate("/admin", {
            replace: true,
          });
        } else {
          if (data.msg === "No hay archivos que subir - validarArchivoSubir") {
            toast.success(`No se cargo o modifico Imagen`, {
              position: toast.POSITION.BOTTOM_LEFT,
              pauseOnHover: false,
            });
          } else {
            toast.error(`${data.msg}`, {
              position: toast.POSITION.BOTTOM_LEFT,
              pauseOnHover: false,
            });
          }
        }
      }
    });
  };

  const updateCoursesFromApi = () => {
    const formData = new FormData();
    formData.append("archivo", selectedFile);
    updatCoursesbyId(form, data._id).then((data) => {
      if (data.status == 1) {
        updateCoursesImageFromApi(formData, data.curso._id);

        toast.success(`Dato Actualizado`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
      } else if (data.msg != undefined) {
        setSelectedFile(undefined);

        if (data.msg === "Token no válido") {
          isToken = true;

          dispatch({ type: types.logoutAdmin });
          navigate("/admin", {
            replace: true,
          });
        } else {
          toast.error(`${data.msg}`, {
            position: toast.POSITION.BOTTOM_LEFT,
            autoClose: 3000,
            pauseOnHover: false,
          });
        }
      } else {
        setSelectedFile(undefined);

        toast.error(`${data.errors[0].msg}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
      }
    });
  };

  const getCategoriesFromApi = () => {
    getCategories().then((data) => {
      if (data.categorias) {
        setCategories(data.categorias);
        console.log(data.categorias);
      }
    });
  };
  const getUsersFromApi = () => {
    getUsers().then(({ usuarios }) => {
      if (usuarios) {
        getTeachersFromApi(usuarios);
      }
    });
  };

  const getTeachersFromApi = (usuarios) => {
    let filterTeachers = usuarios.filter(
      (teacher) => teacher.activeInstructor == true
    );
    setTeachers(filterTeachers);
  };
  const handleForm = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const handleSave = () => {
    createCourseFromApi();
    handleClose();
  };
  const handleEdit = () => {
    updateCoursesFromApi();
    handleClose();
  };
  const imageHandler = (e) => {
    setSelectedFile(e.target.files[0]);

    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        setImageFile(reader.result);
      }
    };
    if (e.target.files[0]) {
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  function isObjEmpty(obj) {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }

    return true;
  }
  const onSwitchAction = () => {
    setIsSwitchOn(!isSwitchOn);
    setForm({ ...form, outstanding: !isSwitchOn });
  };
  return (
    <>
      <ToastContainer autoClose={1500} />

      <Modal show={show} onHide={handleClose} className="AdminModalCourses">
        <Modal.Header closeButton>
          <Modal.Title className=" AdminModalCourses__form__datos__title">
            {type == "CREAR" ? "Crear " : type == "EDITAR" ? "Editar " : "Ver "}
            Curso
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            {type == "VER" ? (
              ""
            ) : (
              <div className=" AdminModalCourses__form__datos__icons">
                <label
                  className={
                    "AdminModalCourses__form__datos__icons__icon camera"
                  }
                  for="fileupload"
                >
                  <AddAPhotoIcon />
                </label>

                <input
                  type="file"
                  id="fileupload"
                  onChange={imageHandler}
                  style={{ visibility: "hidden", display: "none" }}
                />
              </div>
            )}
            <div className="AdminModalCourses__form__datos__image">
              <img
                src={
                  (data.img == undefined || data.img == "") &&
                  selectedFile === undefined
                    ? avatar
                    : selectedFile === undefined
                    ? data.img
                    : imageFile
                }
                alt=""
                style={{ width: "100px" }}
              />
            </div>

            <Form.Group className="mb-4" controlId="exampleForm.ControlInput1">
              <Form.Label>Nombre de Curso*</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ingresa un nombre"
                autoFocus
                onChange={handleForm}
                name="nameCourse"
                disabled={type == "VER" ? true : false}
                value={`${form.nameCourse}`}
              />
            </Form.Group>

            <Form.Group
              className="mb-4"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Descripción</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                onChange={handleForm}
                name="description"
                disabled={type == "VER" ? true : false}
                value={`${form.description}`}
              />
            </Form.Group>
            <Form.Group className="mb-4" controlId="exampleForm.ControlInput1">
              <Form.Label>Youtube Playlist</Form.Label>
              <Form.Control
                type="text"
                placeholder="Ingresa un link"
                autoFocus
                onChange={handleForm}
                name="LinkYoutube"
                disabled={type == "VER" ? true : false}
                value={`${form.LinkYoutube}`}
              />
            </Form.Group>
            <Form.Group
              className="AdminModalCourses__form__datos__listCourse mb-4"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Seleccione una Categoría*</Form.Label>

              <Dropdown>
                <Dropdown.Toggle
                  variant="primary"
                  id="dropdown-basic"
                  disabled={type == "VER" ? true : false}
                >
                  {categorieSelect == ""
                    ? "Lista de Categorías"
                    : categorieSelect}
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  {categories.map((categoria) => {
                    return (
                      <Dropdown.Item
                        key={categoria._id}
                        onClick={() => {
                          setForm({ ...form, category: `${categoria._id}` });
                          setCategorieSelect(categoria.nameCategory);
                          console.log(categoria);
                        }}
                      >
                        {categoria.nameCategory}
                      </Dropdown.Item>
                    );
                  })}
                </Dropdown.Menu>
              </Dropdown>
            </Form.Group>
            <Form.Group
              className="AdminModalCourses__form__datos__listCourse mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Seleccione un Instructor*</Form.Label>

              <Dropdown>
                <Dropdown.Toggle
                  variant="primary"
                  id="dropdown-basic"
                  disabled={type == "VER" ? true : false}
                >
                  {usersSelect == "" ? "Lista de Instructores" : usersSelect}
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  {teachers.map((usuario) => {
                    return (
                      <Dropdown.Item
                        key={usuario.uid}
                        onClick={() => {
                          setForm({
                            ...form,
                            instructor: `${usuario.uid}`,
                          });
                          setUsersSelect(usuario.lastNames);
                          console.log(usuario);
                        }}
                      >
                        {usuario.lastNames}
                      </Dropdown.Item>
                    );
                  })}
                </Dropdown.Menu>
              </Dropdown>
            </Form.Group>
            <Form.Group>
              <Form.Check
                type="switch"
                id="custom-switch"
                label="¿Curso Destacado?"
                checked={isSwitchOn}
                onChange={onSwitchAction}
                disabled={type == "VER" ? true : false}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <button className="button__colorGray" onClick={handleClose}>
            Cerrar
          </button>
          {type == "VER" ? (
            ""
          ) : type == "CREAR" ? (
            <button
              className="button"
              onClick={handleSave}
              disabled={
                form.nameCourse.length > 0 &&
                form.category.length > 0 &&
                form.instructor.length > 0
                  ? false
                  : true
              }
            >
              Guardar
            </button>
          ) : (
            <button
              className="button"
              onClick={handleEdit}
              disabled={
                form.nameCourse.length > 0 &&
                form.category.length > 0 &&
                form.instructor.length > 0
                  ? false
                  : true
              }
            >
              Guardar
            </button>
          )}
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default AdminModalCoursesPage;
