import { css } from "@emotion/react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import TextField from "@mui/material/TextField";
import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import RingLoader from "react-spinners/RingLoader";
import { toast, ToastContainer } from "react-toastify";
import { ReactComponent as Logo } from "../../assets/svg/logo_2.svg";
import { AuthContext } from "../../auth/authContext";
import { loginUser } from "../../services/LoginService";
import { types } from "../../types/types";
import ParticleTwo from "../../components/ParticleTwo/ParticleTwo";
import { Fade } from "react-reveal";

import "./AdmLogin.scss";

const theme = createTheme({
  palette: {
    primary: {
      main: "rgb(80, 43, 207)",
    },
  },
});
const override = css`
  display: block;
  margin: 0 auto;
  border-color: "#fff";
`;

const AdmLogin = () => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const { dispatch } = useContext(AuthContext);

  const getUserAdminFromApi = (form) => {
    loginUser(form).then((data) => {
      const dataAdmin = data.usuario;

      if (data.status === 1) {
        console.log(dataAdmin.rol);

        if (dataAdmin.rol == "ADMIN_ROLE") {
          setLoading(false);
          const adminQPCode = {
            idUser: dataAdmin.uid,
            names: dataAdmin.names,
            token: data.token,
            rol: dataAdmin.rol,
            img: dataAdmin.img,
            email: dataAdmin.email,
          };
          const action = {
            type: types.loginAdmin,
            payload: { adminQPCode },
          };
          dispatch(action);
          navigate("/admin/users", {
            replace: true,
          });
        } else {
          setLoading(false);
          toast.error(`Acceso solo para administradores`, {
            position: toast.POSITION.BOTTOM_LEFT,
            autoClose: 3000,
            pauseOnHover: false,
          });
        }
      } else {
        console.log(data);

        setLoading(false);
        toast.error(`${data.msg}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
        setError(data.msg);
      }
    });
  };

  const handleLoginAdmin = async (form) => {
    setLoading(true);
    getUserAdminFromApi(form);
  };

  const [form, setForm] = useState({ email: "", password: "" });
  const handleForm = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <>
      <ToastContainer autoClose={1500} />

      <div className="adminLogin">
        <Fade left>
          <div className="adminLogin__form">
            <form
              onSubmit={(e) => {
                e.preventDefault();
                handleLoginAdmin(form);
              }}
            >
              <Logo className="adminLogin__form__logo" fill="white" />
              <div className="adminLogin__form__tittle">Iniciar Sesión</div>
              <div className="adminLogin__form__subtittle">
                Ingresa tus credenciales
              </div>
              <ThemeProvider theme={theme}>
                <TextField
                  id="admin-login-email"
                  label="Email"
                  variant="outlined"
                  name="email"
                  fullWidth
                  type="email"
                  className="mb-5 textfield"
                  value={form.email}
                  placeholder="Ingresa tu correo electrónico"
                  focused
                  disabled={loading}
                  onChange={handleForm}
                />
                <TextField
                  id="admin-login-password"
                  label="Password"
                  variant="outlined"
                  name="password"
                  fullWidth
                  type="password"
                  placeholder="Ingresa tu contraseña"
                  className="mb-5"
                  focused
                  value={form.password}
                  disabled={loading}
                  onChange={handleForm}
                />
              </ThemeProvider>
              <div className="adminLogin__form__button">
                {form.password === "" || form.email === "" ? (
                  <button type="button" className="button" disabled>
                    Ingresar
                  </button>
                ) : (
                  <button
                    className="button"
                    disabled={loading}
                    data-test-id="adminLogin-btn-ingresar"
                  >
                    {loading ? (
                      <RingLoader color={"#fff"} css={override} size={40} />
                    ) : (
                      "Ingresar"
                    )}
                  </button>
                )}
              </div>
            </form>
          </div>
        </Fade>
      </div>
    </>
  );
};
export default AdmLogin;
