import React, { useContext, useEffect } from "react";
import { useState } from "react";
import TextField from "@mui/material/TextField";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { purple } from "@mui/material/colors";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import GridLoader from "react-spinners/GridLoader";
import ParticleTwo from "../../components/ParticleTwo/ParticleTwo";
import AddAPhotoIcon from "@material-ui/icons/AddAPhoto";
import { AuthContext } from "../../auth/authContext";
import { types } from "../../types/types";
import { css } from "@emotion/react";
import "react-toastify/dist/ReactToastify.css";
import { Fade } from "react-reveal";

import "./PerfilPage.scss";
import {
  updateUserImagebyId,
  updateUserbyId,
  getUserbyId,
} from "../../services/UserService";
import { userQPCode } from "../../models/userQPCode";
import avatar from "../../assets/img/avatar3.svg";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: "#fff";
`;

const theme = createTheme({
  palette: {
    primary: {
      main: "rgb(80, 43, 207)",
    },
  },
});
let isToken = false;

const PerfilPage = () => {
  const navigate = useNavigate();

  const [editStatus, setEditStatus] = useState(true);
  const [editText, setEditText] = useState("Editar");
  const [error, setError] = useState(false);
  const [form, setForm] = useState({});

  const [loadingPage, setLoadingPage] = useState(true);
  const { user } = useContext(AuthContext);
  const { dispatch } = useContext(AuthContext);

  const [selectedFile, setSelectedFile] = useState();

  const [imageFile, setImageFile] = useState(avatar);

  const getUserFromApi = async () => {
    getUserbyId(user.userQPCode.idUser).then((data) => {
      userQPCode.idUser = data.uid;
      userQPCode.names = data.names;
      userQPCode.lastNames = data.lastNames;
      userQPCode.career = data.career;
      userQPCode.university = data.university;
      userQPCode.actualWork = data.actualWork;
      userQPCode.nameCompany = data.nameCompany;
      userQPCode.linkCV = data.linkCV;
      userQPCode.description = data.description;
      userQPCode.redLinkedin = data.redLinkedin;
      userQPCode.redGitLab = data.redGitLab;
      userQPCode.redGitHub = data.redGitHub;
      userQPCode.redYoutube = data.redYoutube;
      userQPCode.redInsta = data.redInsta;
      userQPCode.redFace = data.redFace;
      userQPCode.TecnologyOne = data.TecnologyOne;
      userQPCode.TecnologyTwo = data.TecnologyTwo;
      userQPCode.TecnologyThree = data.TecnologyThree;
      userQPCode.activeMember = data.activeMember;
      userQPCode.activeInstructor = data.activeInstructor;
      userQPCode.img = data.img;
      userQPCode.phone = data.phone;
      userQPCode.address = data.address;
      userQPCode.email = data.email;
      userQPCode.rol = data.rol;

      const action = {
        type: types.loginUser,
        payload: {
          userQPCode: {
            idUser: data.uid,
            names: data.names,
            token: user.userQPCode.token,
            rol: data.rol,
          },
        },
      };

      if (!isToken) {
        dispatch(action);
      }

      setLoadingPage(false);
      isToken = false;
    });
  };

  useEffect(() => {
    getUserFromApi();
  }, [loadingPage]);

  if (user.userQPCode === undefined) {
    dispatch({ type: types.logoutUser });

    navigate("/login", {
      replace: true,
    });
  }

  const updateUserFromApi = (form) => {
    const formData = new FormData();
    formData.append("archivo", selectedFile);

    updateUserbyId(form, user.userQPCode.idUser).then((data) => {
      if (data.status === 1) {
        updateUserImageFromApi(formData);

        toast.success(`Datos Actualizados`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
        // dispatch(action);
      } else {
        setSelectedFile(undefined);

        if (data.msg === "Token no válido") {
          isToken = true;

          dispatch({ type: types.logoutUser });

          navigate("/login", {
            replace: true,
          });
        } else {
          setForm({
            names: user.userQPCode.names,
            lastNames: userQPCode.lastNames,
            career: userQPCode.career,
            university: userQPCode.university,
            phone: userQPCode.phone,
            address: userQPCode.address,
            actualWork: userQPCode.actualWork,
            nameCompany: userQPCode.nameCompany,
            linkCV: userQPCode.linkCV,
            description: userQPCode.description,
            redLinkedin: userQPCode.redLinkedin.type,
            redGitLab: userQPCode.redGitLab,
            redGitHub: userQPCode.redGitHub,
            redYoutube: userQPCode.redYoutube,
            redInsta: userQPCode.redInsta,
            redFace: userQPCode.redFace,
            TecnologyOne: userQPCode.TecnologyOne,
            TecnologyTwo: userQPCode.TecnologyTwo,
            TecnologyThree: userQPCode.TecnologyThree,
            activeMember: userQPCode.activeMember,
            activeInstructor: userQPCode.activeInstructor,
            img: userQPCode.img,
            email: userQPCode.email,
          });
          toast.error(`${data.errors[0].msg}`, {
            position: toast.POSITION.BOTTOM_LEFT,
            pauseOnHover: false,
          });

          setError(data.errors[0].msg);
        }
      }
    });
  };

  const updateUserImageFromApi = (image) => {
    updateUserImagebyId(image, user.userQPCode.idUser).then((data) => {
      if (data.status === 1) {
        toast.success(`Imagen Perfil Actualizada`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
      } else {
        if (data.msg === "Token no válido") {
          isToken = true;

          dispatch({ type: types.logoutUser });

          navigate("/login", {
            replace: true,
          });
        } else {
          console.log(data.msg);

          if (data.msg === "No hay archivos que subir - validarArchivoSubir") {
            toast.success(`Imagen de Perfil sin  cambios`, {
              position: toast.POSITION.BOTTOM_LEFT,
              pauseOnHover: false,
            });
          } else {
            toast.error(`${data.msg}`, {
              position: toast.POSITION.BOTTOM_LEFT,
              pauseOnHover: false,
            });
          }
        }
      }
    });
  };

  const handleUpdate = async (form) => {
    setEditStatus(!editStatus);
    if (editStatus === true) {
      setEditText("Guardar");
      setForm({
        names: userQPCode.names,
        // phone: userQPCode.phone,
        // lastNames: userQPCode.lastNames,
        // address: userQPCode.address,
        rol: userQPCode.rol,
      });
    } else {
      setEditText("Editar");
      setLoadingPage(true);
      updateUserFromApi(form);
    }
  };

  const handleForm = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const imageHandler = (e) => {
    setSelectedFile(e.target.files[0]);

    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        setImageFile(reader.result);
      }
    };
    if (e.target.files[0]) {
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  return (
    <>
      <div className="perfil">
        <ToastContainer autoClose={2000} />
        {loadingPage ? (
          <div className="perfil__loading">
            <GridLoader color={"white"} size={30} />
          </div>
        ) : (
          <>
            <Fade>
              <form
                className="perfil__form"
                onSubmit={(e) => {
                  e.preventDefault();
                  handleUpdate(form);
                }}
              >
                <ThemeProvider theme={theme}>
                  <div className="perfil__form__title">Perfil</div>
                  <div className="perfil__form__subtitle mb-3">
                    Usted está visualizando sus datos personales.
                  </div>
                  <div className="row perfil__form__datos">
                    <div className="col col-12 ">
                      <div className="perfil__avatar">
                        <img
                          src={
                            user.userQPCode === undefined ||
                            userQPCode.img == ""
                              ? imageFile
                              : selectedFile === undefined
                              ? userQPCode.img
                              : imageFile
                          }
                          alt=""
                        />
                      </div>
                    </div>
                    <div className=" perfil__form__datos__icons">
                      <label
                        className={
                          editStatus || loadingPage
                            ? "perfil__form__datos__icons__icon"
                            : "perfil__form__datos__icons__icon camera"
                        }
                        for="fileupload"
                      >
                        <AddAPhotoIcon />
                      </label>

                      <input
                        disabled={editStatus || loadingPage}
                        type="file"
                        id="fileupload"
                        onChange={imageHandler}
                        style={{ visibility: "hidden", display: "none" }}
                        data-cy="perfil-imageFile-input"
                      />
                    </div>
                  </div>

                  <div className="row perfil__form__datos">
                    <div className=" col col-12 col-md-6 perfil__form__datos__left">
                      <TextField
                        id="perfil-names"
                        label="Nombres"
                        variant="outlined"
                        name="names"
                        fullWidth
                        type="text"
                        placeholder="Ingresa tu Nombre"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined ? "" : userQPCode.names
                        }
                        value={form.names}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                    <div className="col col-12 col-md-6 perfil__form__datos__right">
                      <TextField
                        id="perfil-lastNames"
                        label="Apellidos"
                        variant="outlined"
                        name="lastNames"
                        fullWidth
                        type="text"
                        placeholder="Ingresa tus Apellidos"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.lastNames
                        }
                        value={form.lastNames}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                  </div>
                  <div className="row perfil__form__datos">
                    <div className=" col col-12 col-md-6 perfil__form__datos__left">
                      <TextField
                        id="outlined-required"
                        label="Telefono"
                        variant="outlined"
                        name="phone"
                        fullWidth
                        type="number"
                        placeholder="Ingresa tu Telefono"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined ? "" : userQPCode.phone
                        }
                        value={form.phone}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                    <div className="col col-12 col-md-6 perfil__form__datos__right">
                      <TextField
                        id="outlined-required"
                        label="Direccion"
                        variant="outlined"
                        name="address"
                        fullWidth
                        type="text"
                        placeholder="Ingresa tu Dirección"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.address
                        }
                        value={form.address}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                  </div>
                  <div className="row perfil__form__datos">
                    <div className=" col col-12 col-md-6 perfil__form__datos__left">
                      <TextField
                        id="outlined-required"
                        label="Empresa actual"
                        variant="outlined"
                        name="nameCompany"
                        fullWidth
                        type="text"
                        placeholder="Empresa actual"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.nameCompany
                        }
                        value={form.nameCompany}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                    <div className="col col-12 col-md-6 perfil__form__datos__right">
                      <TextField
                        id="outlined-required"
                        label="Puesto actual"
                        variant="outlined"
                        name="actualWork"
                        fullWidth
                        type="text"
                        placeholder="Puesto actual"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.actualWork
                        }
                        value={form.actualWork}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                  </div>
                  <div className="row perfil__form__datos">
                    <div className=" col col-12 col-md-6 perfil__form__datos__left">
                      <TextField
                        id="outlined-required"
                        label="Universidad o Instituto"
                        variant="outlined"
                        name="university"
                        fullWidth
                        type="text"
                        placeholder="Universidad o Instituto"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.university
                        }
                        value={form.university}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                    <div className="col col-12 col-md-6 perfil__form__datos__right">
                      <TextField
                        id="outlined-required"
                        label="Carrera profesional"
                        variant="outlined"
                        name="career"
                        fullWidth
                        type="text"
                        placeholder="Carrera profesional"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined ? "" : userQPCode.career
                        }
                        value={form.career}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                  </div>

                  <div className="row  perfil__form__datos">
                    <div className="col col-12 ">
                      <TextField
                        id="perfil-description"
                        label="Descripción"
                        variant="outlined"
                        name="description"
                        fullWidth
                        type="text"
                        placeholder="Ingresa tu Descripción"
                        className="mb-4"
                        rows={4}
                        multiline
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.description
                        }
                        value={form.description}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                  </div>
                  <div className="row  perfil__form__datos">
                    <div className="col col-12 ">
                      <TextField
                        id="outlined-required"
                        label="Curriculum vitae"
                        variant="outlined"
                        name="linkCV"
                        fullWidth
                        type="text"
                        placeholder="Ingresa tu link de CV"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined ? "" : userQPCode.linkCV
                        }
                        value={form.linkCV}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                  </div>
                  <div className="row  perfil__form__datos">
                    <div className="col col-12 ">
                      <TextField
                        id="outlined-required"
                        label="Email"
                        variant="outlined"
                        name="email"
                        fullWidth
                        type="email"
                        placeholder="Ingresa tu Correo Electrónico"
                        className="mb-5"
                        defaultValue={
                          user.userQPCode === undefined ? "" : userQPCode.email
                        }
                        value={form.email}
                        onChange={handleForm}
                        focused
                        disabled
                      />
                    </div>
                  </div>

                  <div className="row perfil__form__datos">
                    <div className="perfil__form__subtitle2 mb-5">
                      Tecnologías (Angular, React, Python, Excel, SAP, entre
                      otros).
                    </div>
                    <div className=" col col-12 col-md-4 perfil__form__datos__left">
                      <TextField
                        id="outlined-required"
                        label="Tecnología N°1"
                        variant="outlined"
                        name="TecnologyOne"
                        fullWidth
                        type="text"
                        placeholder="Tecnología N°1"
                        className="mb-5"
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.TecnologyOne
                        }
                        value={form.TecnologyOne}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                    <div className="col col-12 col-md-4 perfil__form__datos__right">
                      <TextField
                        id="outlined-required"
                        label="Tecnología N°2"
                        variant="outlined"
                        name="TecnologyTwo"
                        fullWidth
                        type="text"
                        placeholder="Tecnología N°2"
                        className="mb-5"
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.TecnologyTwo
                        }
                        value={form.TecnologyTwo}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                    <div className="col col-12 col-md-4 perfil__form__datos__right">
                      <TextField
                        id="outlined-required"
                        label="Tecnología N°3"
                        variant="outlined"
                        name="TecnologyThree"
                        fullWidth
                        type="text"
                        placeholder="Tecnología N°3"
                        className="mb-5"
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.TecnologyThree
                        }
                        value={form.TecnologyThree}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                  </div>
                  <div className="row perfil__form__datos">
                    <div className="perfil__form__subtitle2 mb-5">
                      Redes sociales:
                    </div>
                    <div className=" col col-12 col-md-4 perfil__form__datos__left">
                      <TextField
                        id="outlined-required"
                        label="Linkedin"
                        variant="outlined"
                        name="redLinkedin"
                        fullWidth
                        type="text"
                        placeholder="Ingresa tu Linkedin"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.redLinkedin
                        }
                        value={form.redLinkedin}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                    <div className="col col-12 col-md-4 perfil__form__datos__right">
                      <TextField
                        id="outlined-required"
                        label="GitLab"
                        variant="outlined"
                        name="redGitLab"
                        fullWidth
                        type="text"
                        placeholder="Ingresa tu GitLab"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.redGitLab
                        }
                        value={form.redGitLab}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                    <div className="col col-12 col-md-4 perfil__form__datos__right">
                      <TextField
                        id="outlined-required"
                        label="GitHub"
                        variant="outlined"
                        name="redGitHub"
                        fullWidth
                        type="text"
                        placeholder="Ingresa tu GitHub"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.redGitHub
                        }
                        value={form.redGitHub}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                  </div>
                  <div className="row perfil__form__datos">
                    <div className=" col col-12 col-md-4 perfil__form__datos__left">
                      <TextField
                        id="outlined-required"
                        label="Youtube"
                        variant="outlined"
                        name="redYoutube"
                        fullWidth
                        type="text"
                        placeholder="Ingresa tu Youtube"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.redYoutube
                        }
                        value={form.redYoutube}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                    <div className="col col-12 col-md-4 perfil__form__datos__right">
                      <TextField
                        id="outlined-required"
                        label="Instagram"
                        variant="outlined"
                        name="redInsta"
                        fullWidth
                        type="text"
                        placeholder="Ingresa tu Instagram"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.redInsta
                        }
                        value={form.redInsta}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                    <div className="col col-12 col-md-4 perfil__form__datos__right">
                      <TextField
                        id="outlined-required"
                        label="Facebook"
                        variant="outlined"
                        name="redFace"
                        fullWidth
                        type="text"
                        placeholder="Ingresa tu Facebook"
                        className="mb-4"
                        defaultValue={
                          user.userQPCode === undefined
                            ? ""
                            : userQPCode.redFace
                        }
                        value={form.redFace}
                        onChange={handleForm}
                        focused
                        disabled={editStatus || loadingPage}
                      />
                    </div>
                  </div>
                  <div className="row perfil__form__datos  mt-4 mt-md-5">
                    <div className=" perfil__form__button">
                      <button
                        className="button perfil__form__button__textButton "
                        data-test-id="perfil-btn-EditSave"
                      >
                        {editText}
                      </button>
                    </div>
                  </div>
                </ThemeProvider>
              </form>
            </Fade>
          </>
        )}
      </div>
    </>
  );
};

export default PerfilPage;
