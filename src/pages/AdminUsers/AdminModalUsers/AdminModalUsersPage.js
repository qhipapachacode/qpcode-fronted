import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Modal, Form, Dropdown } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";
import { AuthContext } from "../../../auth/authContext";
import AddAPhotoIcon from "@material-ui/icons/AddAPhoto";
import TextField from "@mui/material/TextField";
import { createTheme, ThemeProvider } from "@mui/material/styles";

import { types } from "../../../types/types";
import avatar from "../../../assets/img/avatar3.svg";

import {
  registerUser,
  updateUserbyId,
  updateUserImagebyId,
} from "../../../services/UserAdminService";

import "./AdminModalUsersPage.scss";
const theme = createTheme({
  palette: {
    primary: {
      main: "rgb(80, 43, 207)",
    },
  },
});
let isToken = false;

const roles = ["ADMIN_ROLE", "USER_ROLE"];

const AdminModalUsersPage = ({
  show,
  handleClose,
  handleReload,
  type,
  data,
}) => {
  const navigate = useNavigate();
  const { dispatch } = useContext(AuthContext);
  const [form, setForm] = useState({ nameCourse: `` });
  const [selectedFile, setSelectedFile] = useState();
  const [imageFile, setImageFile] = useState(avatar);
  const [isSwitchOnTeam, setIsSwitchOnTeam] = useState(false);
  const [isSwitchOnTeacher, setIsSwitchOnTeacher] = useState(false);
  const [typeUser, setTypeUser] = useState("");

  useEffect(() => {
    console.log(form.names);
    if (show == true) {
      setSelectedFile(undefined);
      {
        if (isObjEmpty(data) == true) {
          setForm({
            names: ``,
            lastNames: ``,
            career: ``,
            university: ``,
            actualWork: ``,
            nameCompany: ``,
            linkCV: ``,
            address: ``,
            phone: ``,
            description: ``,
            redLinkedin: ``,
            redGitLab: ``,
            redGitHub: ``,
            redYoutube: ``,
            redInsta: ``,
            redFace: ``,
            TecnologyOne: ``,
            TecnologyTwo: ``,
            TecnologyThree: ``,
            email: ``,
            activeMember: false,
            rol: `USER_ROLE`,
            activeInstructor: false,
            password: ``,
            password2: ``,
          });
          setIsSwitchOnTeam(false);
          setIsSwitchOnTeacher(false);
          setTypeUser("");
        } else {
          setForm(data);
          setIsSwitchOnTeam(data.activeMember);
          setIsSwitchOnTeacher(data.activeInstructor);
          setTypeUser(data.rol);
        }
      }
    }
  }, [show]);

  const createUserFromApi = () => {
    const formData = new FormData();
    formData.append("archivo", selectedFile);

    registerUser(form).then((data) => {
      if (data.status == 1) {
        updateUserImageFromApi(formData, data.usuario.uid);

        toast.success(`Usuario ${data.usuario.names} Creado`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
      } else if (data.msg != undefined) {
        setSelectedFile(undefined);

        if (data.msg === "Token no válido") {
          isToken = true;

          dispatch({ type: types.logoutAdmin });
          navigate("/admin", {
            replace: true,
          });
        } else {
          toast.error(`${data.msg}`, {
            position: toast.POSITION.BOTTOM_LEFT,
            autoClose: 3000,
            pauseOnHover: false,
          });
        }
      } else {
        setSelectedFile(undefined);

        toast.error(`${data.errors[0].msg}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
      }
    });
  };

  const updateUserImageFromApi = (image, idCourse) => {
    updateUserImagebyId(image, idCourse).then((data) => {
      if (data.status === 1) {
        toast.success(`Imagen Usuario Actualizado`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
        handleReload();
      } else {
        handleReload();

        if (data.msg === "Token no válido") {
          isToken = true;

          dispatch({ type: types.logoutAdmin });

          navigate("/admin", {
            replace: true,
          });
        } else {
          if (data.msg === "No hay archivos que subir - validarArchivoSubir") {
            toast.success(`No se cargo o modifico Imagen`, {
              position: toast.POSITION.BOTTOM_LEFT,
              pauseOnHover: false,
            });
          } else {
            toast.error(`${data.msg}`, {
              position: toast.POSITION.BOTTOM_LEFT,
              pauseOnHover: false,
            });
          }
        }
      }
    });
  };

  const updateUserFromApi = () => {
    const formData = new FormData();
    formData.append("archivo", selectedFile);
    updateUserbyId(form, data.uid).then((data) => {
      if (data.status == 1) {
        updateUserImageFromApi(formData, data.usuario.uid);

        toast.success(`Dato Actualizado`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
      } else if (data.msg != undefined) {
        setSelectedFile(undefined);

        if (data.msg === "Token no válido") {
          isToken = true;

          dispatch({ type: types.logoutAdmin });
          navigate("/admin", {
            replace: true,
          });
        } else {
          toast.error(`${data.msg}`, {
            position: toast.POSITION.BOTTOM_LEFT,
            autoClose: 3000,
            pauseOnHover: false,
          });
        }
      } else {
        setSelectedFile(undefined);

        toast.error(`${data.errors[0].msg}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
      }
    });
  };

  const handleForm = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const handleSave = () => {
    if (form.password == form.password2) {
      createUserFromApi();
      handleClose();
    } else {
      toast.error("Contraseñas no coinciden", {
        position: toast.POSITION.BOTTOM_LEFT,
        autoClose: 3000,
        pauseOnHover: false,
      });
    }
  };
  const handleEdit = () => {
    updateUserFromApi();
    handleClose();
  };
  const imageHandler = (e) => {
    setSelectedFile(e.target.files[0]);

    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        setImageFile(reader.result);
      }
    };
    if (e.target.files[0]) {
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  function isObjEmpty(obj) {
    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) return false;
    }
    return true;
  }
  const onSwitchActionTeam = () => {
    setIsSwitchOnTeam(!isSwitchOnTeam);
    setForm({ ...form, activeMember: !isSwitchOnTeam });
  };

  const onSwitchActionTeacher = () => {
    setIsSwitchOnTeacher(!isSwitchOnTeacher);
    setForm({ ...form, activeInstructor: !isSwitchOnTeacher });
  };

  return (
    <>
      <ToastContainer autoClose={1500} />

      <Modal show={show} onHide={handleClose} className="AdminModalUsers">
        <Modal.Header closeButton>
          <Modal.Title className=" AdminModalUsers__form__datos__title">
            {type == "CREAR" ? "Crear " : type == "EDITAR" ? "Editar " : "Ver "}
            Usuario
          </Modal.Title>
        </Modal.Header>
        <ThemeProvider theme={theme}>
          <Modal.Body>
            <Form>
              {type == "VER" ? (
                ""
              ) : (
                <div className=" AdminModalUsers__form__datos__icons">
                  <label
                    className={
                      "AdminModalUsers__form__datos__icons__icon camera"
                    }
                    for="fileupload"
                  >
                    <AddAPhotoIcon />
                  </label>

                  <input
                    type="file"
                    id="fileupload"
                    onChange={imageHandler}
                    style={{ visibility: "hidden", display: "none" }}
                  />
                </div>
              )}
              <div className="AdminModalUsers__form__datos__image">
                <img
                  src={
                    (data.img == undefined || data.img == "") &&
                    selectedFile === undefined
                      ? avatar
                      : selectedFile === undefined
                      ? data.img
                      : imageFile
                  }
                  alt=""
                  style={{ width: "100px" }}
                />
              </div>

              <div className="row AdminModalUsers__form__datos__info">
                <div className=" col col-12 col-md-6 ">
                  <TextField
                    id="outlined-required"
                    label="Nombres*"
                    variant="outlined"
                    name="names"
                    fullWidth
                    type="text"
                    placeholder="Ingresa tu Nombre"
                    className="mb-4"
                    value={`${form.names}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
                <div className="col col-12 col-md-6 ">
                  <TextField
                    id="outlined-required"
                    label="Apellidos"
                    variant="outlined"
                    name="lastNames"
                    fullWidth
                    type="text"
                    placeholder="Ingresa tus Apellidos"
                    className="mb-4"
                    value={`${form.lastNames}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
              </div>
              <div className="row ">
                <div className=" col col-12 col-md-6 ">
                  <TextField
                    id="outlined-required"
                    label="Telefono"
                    variant="outlined"
                    name="phone"
                    fullWidth
                    type="number"
                    placeholder="Ingresa tu Telefono"
                    className="mb-4"
                    value={`${form.phone}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
                <div className="col col-12 col-md-6 ">
                  <TextField
                    id="outlined-required"
                    label="Direccion"
                    variant="outlined"
                    name="address"
                    fullWidth
                    type="text"
                    placeholder="Ingresa tu Dirección"
                    className="mb-4"
                    value={`${form.address}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
              </div>
              <div className="row">
                <div className=" col col-12 col-md-6">
                  <TextField
                    id="outlined-required"
                    label="Empresa actual"
                    variant="outlined"
                    name="nameCompany"
                    fullWidth
                    type="text"
                    placeholder="Empresa actual"
                    className="mb-4"
                    value={`${form.nameCompany}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
                <div className="col col-12 col-md-6">
                  <TextField
                    id="outlined-required"
                    label="Puesto actual"
                    variant="outlined"
                    name="actualWork"
                    fullWidth
                    type="text"
                    placeholder="Puesto actual"
                    className="mb-4"
                    value={`${form.actualWork}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
              </div>
              <div className="row">
                <div className=" col col-12 col-md-6">
                  <TextField
                    id="outlined-required"
                    label="Universidad o Instituto"
                    variant="outlined"
                    name="university"
                    fullWidth
                    type="text"
                    placeholder="Universidad o Instituto"
                    className="mb-4"
                    value={`${form.university}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
                <div className="col col-12 col-md-6">
                  <TextField
                    id="outlined-required"
                    label="Carrera profesional"
                    variant="outlined"
                    name="career"
                    fullWidth
                    type="text"
                    placeholder="Carrera profesional"
                    className="mb-4"
                    value={`${form.career}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
              </div>

              <div className="row">
                <div className="col col-12 ">
                  <TextField
                    id="outlined-required"
                    label="Descripción"
                    variant="outlined"
                    name="description"
                    fullWidth
                    type="text"
                    placeholder="Ingresa tu Descripción"
                    className="mb-4"
                    rows={4}
                    multiline
                    value={`${form.description}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col col-12 ">
                  <TextField
                    id="outlined-required"
                    label="Curriculum vitae"
                    variant="outlined"
                    name="linkCV"
                    fullWidth
                    type="text"
                    placeholder="Ingresa tu link de CV"
                    className="mb-4"
                    value={`${form.linkCV}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
              </div>
              <div className="row">
                <div className="col col-12 ">
                  <TextField
                    id="outlined-required"
                    label="Email*"
                    variant="outlined"
                    name="email"
                    fullWidth
                    type="email"
                    placeholder="Ingresa tu Correo Electrónico"
                    className="mb-5"
                    value={`${form.email}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" || type == "EDITAR" ? true : false}
                  />
                </div>
              </div>

              <div className="row ">
                <div className="perfil__form__subtitle2 mb-5">
                  Tecnologías (Angular, React, Python, Excel, SAP, entre otros).
                </div>
                <div className=" col col-12 col-md-4 ">
                  <TextField
                    id="outlined-required"
                    label="Tecnología N°1"
                    variant="outlined"
                    name="TecnologyOne"
                    fullWidth
                    type="text"
                    placeholder="Tecnología N°1"
                    className="mb-5"
                    value={`${form.TecnologyOne}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
                <div className="col col-12 col-md-4 ">
                  <TextField
                    id="outlined-required"
                    label="Tecnología N°2"
                    variant="outlined"
                    name="TecnologyTwo"
                    fullWidth
                    type="text"
                    placeholder="Tecnología N°2"
                    className="mb-5"
                    value={`${form.TecnologyTwo}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
                <div className="col col-12 col-md-4 ">
                  <TextField
                    id="outlined-required"
                    label="Tecnología N°3"
                    variant="outlined"
                    name="TecnologyThree"
                    fullWidth
                    type="text"
                    placeholder="Tecnología N°3"
                    className="mb-5"
                    value={`${form.TecnologyThree}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
              </div>
              <div className="row">
                <div className="perfil__form__subtitle2 mb-5">
                  Redes sociales:
                </div>
                <div className=" col col-12 col-md-4">
                  <TextField
                    id="outlined-required"
                    label="Linkedin"
                    variant="outlined"
                    name="redLinkedin"
                    fullWidth
                    type="text"
                    placeholder="Ingresa tu Linkedin"
                    className="mb-4"
                    value={`${form.redLinkedin}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
                <div className="col col-12 col-md-4">
                  <TextField
                    id="outlined-required"
                    label="GitLab"
                    variant="outlined"
                    name="redGitLab"
                    fullWidth
                    type="text"
                    placeholder="Ingresa tu GitLab"
                    className="mb-4"
                    value={`${form.redGitLab}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
                <div className="col col-12 col-md-4">
                  <TextField
                    id="outlined-required"
                    label="GitHub"
                    variant="outlined"
                    name="redGitHub"
                    fullWidth
                    type="text"
                    placeholder="Ingresa tu GitHub"
                    className="mb-4"
                    value={`${form.redGitHub}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
              </div>
              <div className="row">
                <div className=" col col-12 col-md-4">
                  <TextField
                    id="outlined-required"
                    label="Youtube"
                    variant="outlined"
                    name="redYoutube"
                    fullWidth
                    type="text"
                    placeholder="Ingresa tu Youtube"
                    className="mb-4"
                    value={`${form.redYoutube}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
                <div className="col col-12 col-md-4">
                  <TextField
                    id="outlined-required"
                    label="Instagram"
                    variant="outlined"
                    name="redInsta"
                    fullWidth
                    type="text"
                    placeholder="Ingresa tu Instagram"
                    className="mb-4"
                    value={`${form.redInsta}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
                <div className="col col-12 col-md-4">
                  <TextField
                    id="outlined-required"
                    label="Facebook"
                    variant="outlined"
                    name="redFace"
                    fullWidth
                    type="text"
                    placeholder="Ingresa tu Facebook"
                    className="mb-4"
                    value={`${form.redFace}`}
                    onChange={handleForm}
                    focused
                    disabled={type == "VER" ? true : false}
                  />
                </div>
              </div>
              <div className="row mb-5 mt-4">
                <div className="perfil__form__subtitle2 mb-5">
                  Configuraciones:
                </div>
                <div
                  className="col col-12 col-md-6 mt-2"
                  style={{ display: "flex", jus: "center" }}
                >
                  <Form.Group>
                    <Form.Check
                      type="switch"
                      id="custom-switch"
                      label="¿Es del equipo QPCode?"
                      checked={isSwitchOnTeam}
                      onChange={onSwitchActionTeam}
                      disabled={type == "VER" ? true : false}
                    />
                    <Form.Check
                      type="switch"
                      id="custom-switch"
                      label="¿Es Docente de un curso?"
                      checked={isSwitchOnTeacher}
                      onChange={onSwitchActionTeacher}
                      disabled={type == "VER" ? true : false}
                    />
                  </Form.Group>
                </div>
                <div
                  className="col col-12 col-md-6 mt-2"
                  style={{ display: "flex", justifyContent: "center" }}
                >
                  <Dropdown>
                    <Dropdown.Toggle
                      variant="primary"
                      id="dropdown-basic"
                      disabled={type == "VER" ? true : false}
                    >
                      {typeUser == "" ? "Lista de Roles" : typeUser}
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      {roles.map((rol) => {
                        return (
                          <Dropdown.Item
                            key={roles.indexOf(rol)}
                            onClick={() => {
                              setForm({
                                ...form,
                                rol: `${rol}`,
                              });
                              setTypeUser(rol);
                              console.log(rol);
                            }}
                          >
                            {rol}
                          </Dropdown.Item>
                        );
                      })}
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
              {type == "VER" || type == "EDITAR" ? (
                ""
              ) : (
                <div className="row">
                  <div className="col col-12 col-md-6">
                    <TextField
                      id="outlined-basic"
                      label="Password*"
                      variant="outlined"
                      name="password"
                      fullWidth
                      type="password"
                      value={`${form.password}`}
                      className="mb-4"
                      placeholder="Ingresa tu Contraseña"
                      focused
                      onChange={handleForm}
                    />
                  </div>
                  <div className="col col-12 col-md-6">
                    <TextField
                      id="outlined-basic"
                      label="Confirm Password*"
                      variant="outlined"
                      name="password2"
                      fullWidth
                      type="password"
                      value={`${form.password2}`}
                      className="mb-5"
                      placeholder="Confirma tu Contraseña"
                      focused
                      onChange={handleForm}
                    />
                  </div>
                </div>
              )}
            </Form>
          </Modal.Body>
        </ThemeProvider>

        <Modal.Footer>
          <button className="button__colorGray" onClick={handleClose}>
            Cerrar
          </button>
          {type == "VER" ? (
            ""
          ) : type == "CREAR" ? (
            <button
              className="button"
              onClick={handleSave}
              disabled={
                `${form.names}`.length > 0 &&
                `${form.email}`.length > 0 &&
                `${form.password}`.length > 0 &&
                `${form.password2}`.length > 0
                  ? false
                  : true
              }
            >
              Guardar
            </button>
          ) : (
            <button
              className="button"
              onClick={handleEdit}
              disabled={
                `${form.names}`.length > 0 && `${form.email}`.length > 0
                  ? false
                  : true
              }
            >
              Guardar
            </button>
          )}
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default AdminModalUsersPage;
