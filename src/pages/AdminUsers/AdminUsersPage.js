import React from "react";
import { filter } from "lodash";
import { useEffect, useState, useContext } from "react";
import { sentenceCase } from "change-case";
import { ToastContainer, toast } from "react-toastify";
import { Link as RouterLink } from "react-router-dom";
import Particle from "../../components/Particle/Particle";
import { Fade } from "react-reveal";
import { AuthContext } from "../../auth/authContext";
import { types } from "../../types/types";
import { useNavigate } from "react-router-dom";

// material
import {
  Card,
  Table,
  Stack,
  Avatar,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination,
} from "@mui/material";
// components
import Page from "../../components/Page/Page";
import Label from "../../components/Label/Label";
import Scrollbar from "../../components/Scrollbar";
import Iconify from "../../components/Iconify";
import SearchNotFound from "../../components/SearchNotFound/SearchNotFound";
import {
  UserListHead,
  UserListToolbar,
  UserMoreMenu,
} from "../../sections/@dashboard/user";
//
import { getUsers, deleteUserbyId } from "../../services/UserAdminService.js";
import AdminModalUsersPage from "./AdminModalUsers/AdminModalUsersPage";
import "./AdminUsersPage.scss";

// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: "names", label: "Name", alignRight: false },
  { id: "uid", label: "Id User", alignRight: false },
  { id: "email", label: "Email", alignRight: false },
  // { id: "actualWork", label: "Work", alignRight: false },
  { id: "activeMember", label: "Verified", alignRight: false },
  { id: "activeInstructor", label: "Teacher", alignRight: false },
  { id: "rol", label: "Role", alignRight: false },
  { id: "" },
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.names.toLowerCase().indexOf(query.toLowerCase()) !== -1
    );
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function User() {
  const navigate = useNavigate();
  const { user, dispatch } = useContext(AuthContext);
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState("asc");
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState("names");
  const [filterName, setFilterName] = useState("");
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [users, setUsers] = useState([]);

  const [type, setType] = useState("");
  const [show, setShow] = useState(false);
  const [reload, setReload] = useState(false);
  const [data, setData] = useState({});

  const handleReload = () => setReload(true);
  const handleClose = () => setShow(false);
  const handleShow = (valor, row = {}) => {
    setShow(true);
    setReload(false);
    setType(valor);
    setData(row);
  };

  const handleDelete = (row) => {
    setReload(false);
    deleteUsersFromApi(row.uid);
  };

  const getUsersFromApi = () => {
    getUsers().then((data) => {
      console.log(data.usuarios);
      if (data.usuarios) {
        setUsers(data.usuarios);
      }
    });
  };
  const deleteUsersFromApi = (idCourse) => {
    deleteUserbyId(idCourse).then((data) => {
      if (data.status == 1) {
        handleReload();
        toast.success(`Usuario ${data.usuario.names} Eliminado`, {
          position: toast.POSITION.BOTTOM_LEFT,
          pauseOnHover: false,
        });
      } else if (data.msg != undefined) {
        if (data.msg === "Token no válido") {
          dispatch({ type: types.logoutAdmin });
          navigate("/admin", {
            replace: true,
          });
        } else {
          toast.error(`${data.msg}`, {
            position: toast.POSITION.BOTTOM_LEFT,
            autoClose: 3000,
            pauseOnHover: false,
          });
        }
      }
    });
  };

  useEffect(() => {
    getUsersFromApi();
  }, [reload]);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = users.map((n) => n.names);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - users.length) : 0;

  const filteredUsers = applySortFilter(
    users,
    getComparator(order, orderBy),
    filterName
  );

  const isUserNotFound = filteredUsers.length === 0;

  return (
    <>
      <AdminModalUsersPage
        show={show}
        handleClose={handleClose}
        handleReload={handleReload}
        type={type}
        data={data}
      />
      <ToastContainer autoClose={1500} />
      <Page title="User | Minimal-UI">
        <Fade mb={5}>
          <Container sx={{ mb: 4 }}>
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="space-between"
              mb={5}
            >
              <Typography
                variant="h4"
                gutterBottom
                style={{ color: "rgb(80, 43, 207)" }}
              >
                Users
              </Typography>
              <Button
                variant="contained"
                component={RouterLink}
                to="#"
                startIcon={<Iconify icon="eva:plus-fill" />}
                onClick={() => handleShow("CREAR")}
              >
                New User
              </Button>
            </Stack>

            <Card>
              <UserListToolbar
                numSelected={selected.length}
                filterName={filterName}
                onFilterName={handleFilterByName}
              />

              <Scrollbar>
                <TableContainer sx={{ minWidth: 800 }} className="tableInfo">
                  <Table>
                    <UserListHead
                      order={order}
                      orderBy={orderBy}
                      headLabel={TABLE_HEAD}
                      rowCount={users.length}
                      numSelected={selected.length}
                      onRequestSort={handleRequestSort}
                      onSelectAllClick={handleSelectAllClick}
                    />
                    <TableBody>
                      {filteredUsers
                        .slice(
                          page * rowsPerPage,
                          page * rowsPerPage + rowsPerPage
                        )
                        .map((row) => {
                          const {
                            uid,
                            names,
                            rol,
                            actualWork,
                            email,
                            img,
                            activeMember,
                            activeInstructor,
                            lastNames,
                          } = row;
                          const isItemSelected = selected.indexOf(names) !== -1;

                          return (
                            <TableRow
                              hover
                              key={uid}
                              tabIndex={-1}
                              role="checkbox"
                              selected={isItemSelected}
                              aria-checked={isItemSelected}
                            >
                              <TableCell padding="checkbox">
                                <Checkbox
                                  checked={isItemSelected}
                                  onChange={(event) =>
                                    handleClick(event, names)
                                  }
                                />
                              </TableCell>
                              <TableCell
                                component="th"
                                scope="row"
                                padding="none"
                              >
                                <Stack
                                  direction="row"
                                  alignItems="center"
                                  spacing={2}
                                >
                                  <Avatar alt={names} src={img} />
                                  <Typography variant="subtitle2" noWrap>
                                    {names} {lastNames}
                                  </Typography>
                                </Stack>
                              </TableCell>
                              <TableCell align="left">{uid}</TableCell>
                              <TableCell align="left">{email}</TableCell>
                              <TableCell align="left">
                                {activeMember ? "Yes" : "No"}
                              </TableCell>
                              <TableCell align="left">
                                {activeInstructor ? "Yes" : "No"}
                              </TableCell>
                              <TableCell align="left">
                                <Label
                                  variant="ghost"
                                  color={
                                    (rol === "USER_ROLE" && "info") || "success"
                                  }
                                >
                                  {sentenceCase(rol)}
                                </Label>
                              </TableCell>

                              <TableCell align="right">
                                <UserMoreMenu
                                  handleShow={handleShow}
                                  row={row}
                                  handleDelete={handleDelete}
                                />
                              </TableCell>
                            </TableRow>
                          );
                        })}
                      {emptyRows > 0 && (
                        <TableRow style={{ height: 53 * emptyRows }}>
                          <TableCell colSpan={6} />
                        </TableRow>
                      )}
                    </TableBody>
                    {isUserNotFound && (
                      <TableBody>
                        <TableRow>
                          <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                            <SearchNotFound searchQuery={filterName} />
                          </TableCell>
                        </TableRow>
                      </TableBody>
                    )}
                  </Table>
                </TableContainer>
              </Scrollbar>

              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={users.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Card>
          </Container>
        </Fade>
      </Page>
    </>
  );
}
