import React, { useEffect, useState } from "react";
import cursos from "../../data/cursos";
import { OuterLayout } from "../../styles/Layouts";

import HeaderHome from "../../components/HeaderHome/HeaderHome";
import CoursesDest from "../../components/CoursesDest/CoursesDest";
import NoticesDest from "../../components/NoticesDest/NoticesDest";
import BlogDest from "../../components/BlogDest/BlogTest";
import FAQSection from "../../components/FAQSection/FAQSection";
import { Fade } from "react-reveal";

const allButtons = ["All", ...new Set(cursos.map((item) => item.category))];

const HomePage = () => {
  const [menuItem, setMenuItems] = useState(cursos);
  const [button, setButtons] = useState(allButtons);
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const filter = (button) => {
    if (button === "All") {
      setMenuItems(cursos);
      return;
    }

    const filteredData = cursos.filter((item) => item.category === button);
    setMenuItems(filteredData);
  };
  return (
    <>
      <HeaderHome />
      <CoursesDest />
      <NoticesDest />
      <FAQSection />
    </>
  );
};

export default HomePage;
