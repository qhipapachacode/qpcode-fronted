import React, { useContext, useState } from "react";
import TextField from "@mui/material/TextField";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import { Link, useNavigate } from "react-router-dom";
import { types } from "../../types/types";
import RingLoader from "react-spinners/RingLoader";
import { AuthContext } from "../../auth/authContext";
import { registerUser } from "../../services/UserService";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { css } from "@emotion/react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./RegisterPage.scss";
import { Fade } from "react-reveal";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: "#fff";
`;

const theme = createTheme({
  palette: {
    primary: {
      main: "rgb(80, 43, 207)",
    },
  },
});

const RegisterPage = () => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);

  const { dispatch } = useContext(AuthContext);

  const [form, setForm] = useState({
    names: "",
    lastNames: "",
    email: "",
    password: "",
    password2: "",
    rol: "USER_ROLE",
  });

  const getUserFromApi = (form) => {
    registerUser(form).then((data) => {
      if (data.status === 1) {
        const dataUsuario = data.usuario;

        setLoading(false);
        const userQPCode = {
          idUser: dataUsuario.uid,
          names: dataUsuario.names,
          token: data.token,
        };

        const action = {
          type: types.loginUser,
          payload: { userQPCode },
        };

        dispatch(action);
        navigate("/perfil", {
          replace: true,
        });
      } else {
        setLoading(false);
        toast.error(`${data[0].msg}`, {
          position: toast.POSITION.BOTTOM_LEFT,
          autoClose: 3000,
          pauseOnHover: false,
        });
      }
    });
  };

  const handleRegister = async (form) => {
    if (form.password == form.password2) {
      setLoading(true);
      getUserFromApi(form);
    } else {
      toast.error("Contraseñas no coinciden", {
        position: toast.POSITION.BOTTOM_LEFT,
        autoClose: 3000,
        pauseOnHover: false,
      });
    }
  };

  const handleForm = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <>
      <div className="register">
        <ToastContainer autoClose={1500} />
        <Fade>
          <form
            className="register__form"
            onSubmit={(e) => {
              e.preventDefault();
              handleRegister(form);
            }}
          >
            <Link to="/login">
              <div className="register__form__back mb-1">
                <ArrowBackIosIcon />
                Regresar
              </div>
            </Link>

            <div className="register__form__title mb-1">Registro</div>

            <div className="register__form__subtitle mb-5">
              Por favor complete todos los campos para registrarse
            </div>
            <ThemeProvider theme={theme}>
              <div className="row register__form__datos">
                <div className=" col col-12 col-md-6 register__form__datos__left">
                  <TextField
                    id="register-names"
                    label="Names *"
                    variant="outlined"
                    name="names"
                    value={form.names}
                    fullWidth
                    type="text"
                    className="mb-4"
                    placeholder="Ingresa tu Nombre"
                    focused
                    disabled={loading}
                    onChange={handleForm}
                  />
                </div>
                <div className="col col-12 col-md-6 register__form__datos__right">
                  <TextField
                    id="register-lastNames"
                    label="LastNames"
                    variant="outlined"
                    name="lastNames"
                    value={form.lastNames}
                    fullWidth
                    type="text"
                    className="mb-4"
                    placeholder="Ingresa tus Apellidos"
                    focused
                    disabled={loading}
                    onChange={handleForm}
                  />
                </div>
                <div className=" col col-12 col-md-6 register__form__datos__left">
                  <TextField
                    id="register-phone"
                    label="Phone"
                    variant="outlined"
                    name="phone"
                    fullWidth
                    type="number"
                    value={form.phone}
                    className="mb-4"
                    placeholder="Ingresa tu Telefono"
                    focused
                    disabled={loading}
                    onChange={handleForm}
                  />
                </div>
                <div className="col col-12 col-md-6 register__form__datos__right">
                  <TextField
                    id="register-address"
                    label="Address"
                    variant="outlined"
                    name="address"
                    value={form.address}
                    fullWidth
                    type="text"
                    className="mb-4"
                    placeholder="Ingresa tus Dirección"
                    focused
                    disabled={loading}
                    onChange={handleForm}
                  />
                </div>
              </div>
              <div className="row  register__form__datos">
                <div className="col col-12 ">
                  <TextField
                    id="register-email"
                    label="Email *"
                    variant="outlined"
                    name="email"
                    value={form.email}
                    fullWidth
                    type="email"
                    className="mb-4"
                    placeholder="Ingresa tu Correo Electrónico"
                    focused
                    disabled={loading}
                    onChange={handleForm}
                  />
                </div>
              </div>
              <div className="row register__form__datos">
                <div className="col col-12 col-md-6 register__form__datos__left">
                  <TextField
                    id="register-password"
                    label="Password *"
                    variant="outlined"
                    name="password"
                    fullWidth
                    type="password"
                    value={form.password}
                    className="mb-4"
                    placeholder="Ingresa tu Contraseña"
                    focused
                    disabled={loading}
                    onChange={handleForm}
                  />
                </div>
                <div className="col col-12 col-md-6 register__form__datos__right">
                  <TextField
                    id="register-password2"
                    label="Confirm Password *"
                    variant="outlined"
                    name="password2"
                    fullWidth
                    type="password"
                    value={form.password2}
                    className="mb-5"
                    placeholder="Confirma tu Contraseña"
                    focused
                    disabled={loading}
                    onChange={handleForm}
                  />
                </div>
              </div>
              <div className="row register__form__datos">
                <div className=" register__form__datos__button">
                  {form.password === "" ||
                  form.email === "" ||
                  form.password2 === "" ||
                  form.names === "" ? (
                    <button type="button" className="button" disabled>
                      Registrarme
                    </button>
                  ) : (
                    <button
                      className="button register__form__textButton"
                      disabled={loading}
                      data-test-id="register-btn-registrarme"
                    >
                      {loading ? (
                        <RingLoader color={"#fff"} css={override} size={40} />
                      ) : (
                        "Registrarme"
                      )}
                    </button>
                  )}
                </div>
              </div>
            </ThemeProvider>
          </form>{" "}
        </Fade>
      </div>
    </>
  );
};

export default RegisterPage;
