const noticesGrafic = [
  {
    id: 1,
    link: "https://youtu.be/GRDC_7oJ_Ag",
    title: "Arduino Basico",
    dest: true,
  },
  {
    id: 2,
    link: "https://i.imgur.com/9i0pDSv.png",
    title: "Arduino Basico",
    dest: true,
  },
  {
    id: 3,
    link: "https://youtu.be/xF6nNoMFb2w",
    title: "Arduino Basico",
    text: "Instructor: Jhonny Quiliche",
    dest: true,
  },
  {
    id: 4,
    link: "https://i.imgur.com/19Au5c1.png",
    title: "Arduino Basico",
    text: "Instructor: Jhonny Quiliche",
    dest: true,
  },
  {
    id: 5,
    link: "https://youtu.be/zmyJjSU3_TA",
    title: "Arduino Basico",
    text: "Instructor: Jhonny Quiliche",
    dest: true,
  },
  {
    id: 6,
    link: "https://i.imgur.com/lMbFSsC.png",
    title: "Arduino Basico",
    dest: true,
  },
  {
    id: 7,
    link: "https://www.facebook.com/watch/?v=3951908994841937",
    title: "Arduino Basico",
    dest: true,
  },
  {
    id: 8,
    link: "https://i.imgur.com/1nOMVLI.png",
    title: "Arduino Basico",
    dest: true,
  },
  {
    id: 9,
    link: "https://www.facebook.com/watch/?v=2176696629106532",
    title: "Arduino Basico",
    dest: true,
  },
];

export default noticesGrafic;
