import React from "react";
import { Route, Routes } from "react-router-dom";
import ParticleTwo from "../components/ParticleTwo/ParticleTwo";
import Particle from "../components/Particle/Particle";

import PerfilPage from "../pages/Perfil/PerfilPage";
import CursoPage from "../pages/Cursos/Curso/CursoPage";

export const DashboardClient = () => {
  return (
    <>
      <Routes>
        <Route
          path="/perfil"
          exact={true}
          element={
            <>
              <ParticleTwo />
              <PerfilPage />
            </>
          }
        />
        <Route
          path="/courses/:id"
          exact={true}
          element={
            <>
              <Particle />
              <CursoPage />
            </>
          }
        />
      </Routes>
    </>
  );
};
