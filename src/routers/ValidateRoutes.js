import { useEffect, useState } from "react";
import { Route, Routes, useLocation } from "react-router-dom";
import { DashboardClient } from "./DashboardClient";
import { PrivateAdminRoutes } from "./PrivateAdminRoutes";
import { PrivateClientRoutes } from "./PrivateClientRoutes";
import Footer from "../components/Footer/Footer";
import NavBar from "../components/Navbar/Navbar";
import AdmLogin from "../pages/AdminLogin/AdmLogin";
import React from "react";
import HomePage from "../pages/Home/HomePage";
import LoginPage from "../pages/Login/LoginPage";
import RegisterPage from "../pages/Register/RegisterPage";
import RestorePage from "../pages/Restore/RestorePage";
import CursosPage from "../pages/Cursos/CursosPage";
import DashboardLayout from "../pages/AdminDashboard";
import { Adminlayout } from "../styles/Layouts";
import AdminUsersPage from "../pages/AdminUsers/AdminUsersPage";
import AdminCategoriesPage from "../pages/AdminCategories/AdminCategoriesPage";
import AdminCoursesPage from "../pages/AdminCourses/AdminCoursesPage";
import AdminQuestionsPage from "../pages/AdminQuestions/AdminQuestionsPage";
import ParticleTwo from "../components/ParticleTwo/ParticleTwo";
import Particle from "../components/Particle/Particle";

export const ValidateRoutes = () => {
  const [validar, setValidar] = useState(true);
  let location = useLocation();
  let ubication = location.pathname;

  function usePageViews() {
    useEffect(() => {
      setValidar(ubication.includes("/admin"));
    }, []);
  }
  usePageViews();

  return (
    <>
      {!validar && (
        <NavBar ubication={`${ubication.replaceAll("/", "")}-content`} />
      )}

      <Routes>
        {/* Client */}
        <Route path="/" exact={true} element={<HomePage />} />
        <Route path="/login" exact={true} element={<LoginPage />} />
        <Route
          path="/register"
          exact={true}
          element={
            <>
              <ParticleTwo />
              <RegisterPage />
            </>
          }
        />
        <Route
          path="/restore-password"
          exact={true}
          element={
            <>
              <ParticleTwo />
              <RestorePage />
            </>
          }
        />
        <Route path="/courses" exact={true} element={<CursosPage />} />

        {/* admin */}
        <Route
          path="/admin"
          exact={true}
          element={
            <>
              <ParticleTwo />
              <AdmLogin />
            </>
          }
        />

        <Route
          path="/admin/*"
          element={
            <PrivateAdminRoutes>
              <>
                <DashboardLayout />

                <Adminlayout>
                  <Routes>
                    <Route
                      path="/dashboard"
                      exact={true}
                      element={
                        <>
                          <CursosPage />
                        </>
                      }
                    />
                    <Route
                      path="/users"
                      exact={true}
                      element={
                        <>
                          <Particle />
                          <AdminUsersPage />
                        </>
                      }
                    />
                    <Route
                      path="/categories"
                      exact={true}
                      element={
                        <>
                          <Particle />
                          <AdminCategoriesPage />
                        </>
                      }
                    />
                    <Route
                      path="/courses"
                      exact={true}
                      element={
                        <>
                          <Particle />
                          <AdminCoursesPage />
                        </>
                      }
                    />
                    <Route
                      path="/questions"
                      exact={true}
                      element={
                        <>
                          <Particle />
                          <AdminQuestionsPage />
                        </>
                      }
                    />
                  </Routes>
                </Adminlayout>
              </>
            </PrivateAdminRoutes>
          }
        />

        <Route
          path="/*"
          element={
            <PrivateClientRoutes>
              <DashboardClient />
            </PrivateClientRoutes>
          }
        />
      </Routes>
      {!validar && <Footer />}
    </>
  );
};
