import { types } from "../types/types";

export const authReducer = (state = {}, action) => {
  switch (action.type) {
    case types.loginUser:
      return {
        ...action.payload,
        loggedUser: true,
      };

    case types.logoutUser:
      return {
        loggedUser: false,
      };

    case types.loginAdmin:
      return {
        ...action.payload,
        loggedAdmin: true,
      };

    case types.logoutAdmin:
      return {
        loggedAdmin: false,
      };

    default:
      return state;
  }
};
