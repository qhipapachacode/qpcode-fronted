import React from "react";
import YouTubeIcon from "@mui/icons-material/YouTube";
import DoubleArrowIcon from "@mui/icons-material/DoubleArrow";
import { Fade, Flip } from "react-reveal";

import { Link } from "react-router-dom";

import "./MenuCardBlog.scss";

const MenuCardBlog = ({ menuItem }) => {
  const filteredData = menuItem.filter((item) => item.dest === true);

  return (
    <div className="menuCardBlog">
      {filteredData.map((item) => {
        return (
          <Fade key={item.id}>
            <div className="menuCardBlog__grid">
              <div className="menuCardBlog__grid__content">
                <div className="menuCardBlog__grid__content__image">
                  <img src={item.image} alt="" />
                  <ul>
                    {item.link && (
                      <li>
                        <a target="_blank" href={item.link} rel="noreferrer">
                          <YouTubeIcon />
                        </a>
                      </li>
                    )}
                  </ul>
                </div>
                <h6 className="menuCardBlog__grid__content__title">
                  {item.title}
                </h6>
                <p className="menuCardBlog__grid__content__subtitle">
                  {item.text}
                </p>
                <div className="indications__button">
                  <Link to={item.page}>
                    <button
                      type="button"
                      className="menuCardBlog__grid__content__button"
                    >
                      <span>Ver Blog</span>
                      <DoubleArrowIcon style={{ marginLeft: "15x" }} />
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </Fade>
        );
      })}
    </div>
  );
};

export default MenuCardBlog;
