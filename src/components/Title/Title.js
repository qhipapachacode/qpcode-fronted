import React from "react";
import "./Title.scss";

function Title({ title, span }) {
  return (
    <div className="title">
      <h2>
        {title}
        <b>
          <span>{span}</span>
        </b>
      </h2>
    </div>
  );
}

export default Title;
