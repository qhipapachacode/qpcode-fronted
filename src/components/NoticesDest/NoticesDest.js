import React from "react";
import { Fade } from "react-reveal";
import Slides from "../Slides/Slides";
import noticesGrafic from "../../data/noticesGrafic";
import noticesPrensa from "../../data/noticesPrensa";
import SecondaryButton from "../SecondaryButoon/SecondaryButton";

import "./NoticesDest.scss";

function NoticesDest() {
  return (
    <div className="noticesDest">
      <div className="noticesDest__content">
        <Fade right>
          <div className="noticesDest__content__tittle">Notices QPcode</div>
        </Fade>
        <Slides noticesGrafic={noticesGrafic} />

        <Fade>
          <div
            className="noticesDest__content__links"
            data-test-id="home-notices-btnLink-elements"
          >
            {noticesPrensa.map((item) => {
              return (
                <SecondaryButton
                  key={item.id}
                  name={`${item.prensa} ${item.year}`}
                  link={item.link}
                />
              );
            })}
          </div>
        </Fade>
      </div>
    </div>
  );
}

export default NoticesDest;
