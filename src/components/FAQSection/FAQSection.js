import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { InnerLayout } from "../../styles/Layouts";
import Question from "../Question/Question";
import { Fade } from "react-reveal";
import Title from "../Title/Title";
import GridLoader from "react-spinners/GridLoader";
import { getQuestions } from "../../services/QuestionService.js";

const FAQSection = () => {
  const [loadingPage, setLoadingPage] = useState(true);
  const [questions, setQuestions] = useState([]);
  useEffect(() => {
    getCoursesFromApi();
  }, []);

  const getCoursesFromApi = () => {
    setLoadingPage(true);

    getQuestions().then((data) => {
      if (data.questions) {
        setQuestions(data.questions);
        setLoadingPage(false);
      }
    });
  };

  return (
    <Fade left>
      <FaqStyled>
        <InnerLayout>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              width: "100%",
            }}
          >
            <div
              style={{
                width: "80%",
              }}
            >
              <Fade left>
                <Title
                  title={"Preguntas Frecuentes"}
                  span={"Preguntas Frecuentes"}
                />
              </Fade>
              {loadingPage ? (
                <div className="cursoPage__content__loading">
                  <GridLoader color={"rgb(80, 43, 207)"} size={30} />
                </div>
              ) : (
                <div
                  className="questions-con"
                  data-test-id="home-questions-elements"
                >
                  {questions.map((q, i) => {
                    return <Question key={q.id} {...q} indice={i} />;
                  })}
                </div>
              )}
            </div>
          </div>
        </InnerLayout>
      </FaqStyled>
    </Fade>
  );
};

const FaqStyled = styled.section`
  .c-para {
    width: 60%;
    margin: 0 auto;
  }
  .questions-con {
    padding-top: 4rem;
  }
  .lines {
    position: absolute;
    left: 0;
    top: 300%;
    width: 100%;
    z-index: -1;
    img {
      width: 100%;
    }
  }
`;

export default FAQSection;
