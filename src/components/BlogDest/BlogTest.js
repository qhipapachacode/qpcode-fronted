import { Fade, Flip } from "react-reveal";
import React, { useState } from "react";
import Title from "../Title/Title";
import cursos from "../../data/cursos";
import "./BlogTest.scss";
import MenuCardBlog from "../MenuCardBlog/MenuCardBlog";

function BlogTest() {
  return (
    <div className="blogDest">
      <div className="blogDest__content">
        <div className="blogDest__content__tittle">
          <Fade left>
            <Title title={"Blog Destacados"} span={"Blog Destacados"} />
          </Fade>
        </div>
        <Fade>
          <MenuCardBlog menuItem={cursos} />
        </Fade>
      </div>
    </div>
  );
}

export default BlogTest;
