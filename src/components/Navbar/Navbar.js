import React, { useState, useContext, useEffect } from "react";
import { Link, NavLink, useLocation } from "react-router-dom";
import { ReactComponent as Logo } from "../../assets/svg/logo_2.svg";
import { TiThMenuOutline } from "react-icons/ti";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../auth/authContext";
import { types } from "../../types/types";

import "./Navbar.scss";

const Navbar = () => {
  const [validatePerfil, setValidatePerfil] = useState(true);
  const navigate = useNavigate();
  const { user, dispatch } = useContext(AuthContext);
  let location = useLocation();
  let position = location.pathname;

  useEffect(() => {
    setValidatePerfil(position.includes("/perfil"));

    setValidatePerfil(position.includes("/login"));
    console.log(validatePerfil);
  }, []);

  const handleLogout = () => {
    dispatch({ type: types.logoutUser });

    navigate("/login", {
      replace: true,
    });
    setValidatePerfil(true);
  };

  const handlePerfil = () => {
    navigate("/perfil", {
      replace: true,
    });

    setValidatePerfil(true);
  };
  return (
    <nav className="navbar fixed-top navbar-expand-md navbar-dark">
      <Link
        className="navbar-brand"
        to="/"
        onClick={() => setValidatePerfil(false)}
      >
        <Logo className="navbar-logo" fill="white" />
        QPCode
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-bs-toggle="collapse"
        data-bs-target="#navbarNavDropdown"
        aria-controls="navbarNavDropdown"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <TiThMenuOutline color="white" />
      </button>
      <div className="collapse navbar-collapse" id="navbarNavDropdown">
        <div className="navbar-nav">
          <NavLink
            className={({ isActive }) =>
              "nav-item nav-link " + (isActive ? "active" : "")
            }
            onClick={() => setValidatePerfil(false)}
            to="/"
            data-test-id="home-page-button"
          >
            Home
          </NavLink>

          {user.userQPCode !== undefined ? (
            <li className="nav-item dropdown">
              <NavLink
                className="nav-link dropdown-toggle"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                id="navbarDropdown"
                to="/x"
                data-test-id="perfilSelect-page-button"
              >
                <span className={`${validatePerfil ? "avtivare" : ""} `}>
                  {user.userQPCode === undefined
                    ? "Login"
                    : user.userQPCode.names}
                </span>
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                <li>
                  <button
                    className="dropdown-item"
                    onClick={handlePerfil}
                    data-test-id="perfil-page-button"
                  >
                    Mi Perfil
                  </button>
                </li>
                <li>
                  <button
                    className="dropdown-item"
                    onClick={handleLogout}
                    data-test-id="logout-button"
                  >
                    Cerrar Sesion
                  </button>
                </li>
              </ul>
            </li>
          ) : (
            <NavLink
              className={({ isActive }) =>
                "nav-item nav-link " + (isActive ? "active" : "")
              }
              to="/login"
              onClick={() => setValidatePerfil(true)}
              data-test-id="login-page-button"
            >
              Ingresar
            </NavLink>
          )}
        </div>
      </div>
    </nav>
  );
};
export default Navbar;
