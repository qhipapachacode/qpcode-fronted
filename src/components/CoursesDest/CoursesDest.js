import { Fade, Flip } from "react-reveal";
import React, { useEffect, useState } from "react";
import Title from "../Title/Title";
import MenuCard from "../MenuCard/MenuCard";
import GridLoader from "react-spinners/GridLoader";
import "./CoursesDest.scss";

import { getCourses } from "../../services/CourseService.js";

const CoursesDest = () => {
  const [loadingPage, setLoadingPage] = useState(true);
  const [cursos, setCursos] = useState([]);

  useEffect(() => {
    getCoursesFromApi();
  }, []);

  const getCoursesFromApi = () => {
    setLoadingPage(true);

    getCourses().then((data) => {
      if (data.cursos) {
        setCursos(data.cursos);
        setLoadingPage(false);
      }
    });
  };

  return (
    <div className="coursesDest">
      <div className="coursesDest__content">
        <div className="coursesDest__content__tittle">
          <Fade left>
            <Title title={"Cursos Destacados"} span={"Cursos Destacados"} />
          </Fade>
        </div>
        {loadingPage ? (
          <div className="cursoPage__content__loading">
            <GridLoader color={"rgb(80, 43, 207)"} size={30} />
          </div>
        ) : (
          <Fade>
            <MenuCard menuItem={cursos} />
          </Fade>
        )}
      </div>
    </div>
  );
};

export default CoursesDest;
