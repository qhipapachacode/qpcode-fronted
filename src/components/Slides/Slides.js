import { Fade } from "@mui/material";
import React, { useEffect, useState } from "react";
import Title from "../Title/Title";
import cursos from "../../data/cursos";
import { Navigation, Pagination, Scrollbar, A11y } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import ReactPlayer from "react-player";

import "swiper/scss";
import "swiper/scss/navigation";
import "swiper/scss/pagination";
import "swiper/scss/scrollbar";

import "./Slides.scss";

function Slides({ noticesGrafic }) {
  const [play, setPlay] = useState(false);
  const [slide, setSlide] = useState(false);

  return (
    <div className="slides">
      <div className="slides__content">
        <Swiper
          className=""
          // install Swiper modules
          modules={[Navigation, Pagination, Scrollbar, A11y]}
          spaceBetween={50}
          slidesPerView={3}
          navigation
          // loop
          pagination={{ clickable: true }}
          scrollbar={{ draggable: true }}
          onSwiper={(swiper) => (window.swiper = swiper)}
          onSlideChange={() => (setPlay(false), console.log(slide))}
          breakpoints={{
            // when window width is >= 640px
            100: {
              slidesPerView: 1,
            },
            768: {
              slidesPerView: 2,
            },
            998: {
              slidesPerView: 3,
            },
          }}
          data-test-id="home-notices-cards-elements"
        >
          <Fade>
            {noticesGrafic.map((item) => {
              return (
                <SwiperSlide className="slides__content__notices" key={item.id}>
                  {item.link.includes("youtu") ||
                  item.link.includes("facebook") ? (
                    <ReactPlayer
                      url={item.link}
                      className="slides__content__notices__videos"
                      playing={play}
                    />
                  ) : (
                    <img
                      className="slides__content__notices__images"
                      src={item.link}
                      alt=""
                    />
                  )}
                </SwiperSlide>
              );
            })}
          </Fade>
        </Swiper>
      </div>
    </div>
  );
}

export default Slides;
