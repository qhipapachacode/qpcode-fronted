import React from "react";
import DoubleArrowIcon from "@mui/icons-material/DoubleArrow";
import { Link } from "react-router-dom";

import "./SecondaryButton.scss";

const SecondaryButton = ({ name, link = "", qpLink = "" }) => {
  return (
    <>
      {link.includes("http") ? (
        <a style={{ textDecoration: "none" }} target="_blank" href={link}>
          <div className="secondaryButoon">
            {name}
            <DoubleArrowIcon style={{ marginLeft: "5px" }} />
          </div>
        </a>
      ) : (
        <Link to={qpLink}>
          <div className="secondaryButoon" data-test-id="home-header-btnText">
            {name}
            <DoubleArrowIcon style={{ marginLeft: "5px" }} />
          </div>
        </Link>
      )}
    </>
  );
};

export default SecondaryButton;
