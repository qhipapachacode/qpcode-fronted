import React from "react";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import YoutubeIcon from "@material-ui/icons/YouTube";
import { FaGitlab } from "react-icons/fa";
import "./LinksRedes.scss";

function Linksredes() {
  return (
    <div className="linkRedes">
      <div className="linkRedes__icons">
        <a
          className="linkRedes__icons__icon youtube"
          target="_blank"
          href="https://www.youtube.com/channel/UCXfdGhH8ifbMixOaj07mkzQ"
          rel="noreferrer"
        >
          <YoutubeIcon />
        </a>
        <a
          className="linkRedes__icons__icon instagram"
          target="_blank"
          href="https://www.instagram.com/qhipapachacode/"
          rel="noreferrer"
        >
          <InstagramIcon />
        </a>
        <a
          className="linkRedes__icons__icon facebook"
          target="_blank"
          href="https://www.facebook.com/QhipaPachaCode/"
          rel="noreferrer"
        >
          <FacebookIcon />
        </a>
        <a
          className="linkRedes__icons__icon gitlab"
          target="_blank"
          href="https://gitlab.com/jorge_vicuna"
          rel="noreferrer"
        >
          <FaGitlab style={{ width: "25" }} />
        </a>
      </div>
    </div>
  );
}

export default Linksredes;
