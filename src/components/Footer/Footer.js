import React from "react";
import { ReactComponent as Logo } from "../../assets/svg/logo_2.svg";
import Linksredes from "../LinksRedes/LinksRedes";
import "./Footer.scss";
import { Fade } from "react-reveal";

function Footer() {
  return (
    <div className="footer">
      <Fade>
        <div className="footer__text">Desarrollado con ❤️ por QPCode </div>
      </Fade>
      <Fade>
        <Linksredes />
      </Fade>
    </div>
  );
}

export default Footer;
