import React, { useState } from "react";
import styled from "styled-components";
import plus from "../../assets/svg/plus.svg";
import minus from "../../assets/svg/minus.svg";
import { Fade } from "react-reveal";

function Question({ titleQuestion, responseQuestion, indice }) {
  const [toggle, setToggle] = useState(false);

  const btnToggler = () => {
    setToggle(!toggle);
  };

  return (
    <Fade left cascade>
      <QuestionStyled>
        <div className="q-con">
          <div className="toggle-title">
            <h4
              data-test-id={indice === 0 ? "home-questions-element-title" : ""}
            >
              {titleQuestion}
            </h4>
            <button
              onClick={btnToggler}
              data-test-id={indice === 0 ? "home-questions-element-btn" : ""}
            >
              {toggle ? <img src={minus} alt="" /> : <img src={plus} alt="" />}
            </button>
          </div>
          {toggle && (
            <p
              data-test-id={
                indice === 0 ? "home-questions-element-response" : ""
              }
            >
              {responseQuestion}
            </p>
          )}
        </div>
      </QuestionStyled>
    </Fade>
  );
}

const QuestionStyled = styled.div`
  background-color: #fff;
  margin: 1rem 0;
  padding: 1.4rem 1rem;
  border-radius: 24px;
  transition: all 0.4s ease-in-out;
  box-shadow: 0px 25px 50px rgba(22, 25, 79, 0.05);
  p {
    transition: all 0.4s ease-in-out;
    color: #7d7d7d;
  }
  h4 {
    color: #16194f;
    transition: all 0.4s ease-in-out;
    font-size: 1.3rem;
  }
  .toggle-title {
    display: flex;
    align-items: center;
    justify-content: space-between;
    transition: all 0.4s ease-in-out;
    button {
      background: transparent;
      outline: none;
      cursor: pointer;
      border: none;
    }
  }
`;

export default Question;
