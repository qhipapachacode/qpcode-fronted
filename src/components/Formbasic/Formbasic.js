import React, { useState, useContext } from "react";
import TextField from "@mui/material/TextField";
import RingLoader from "react-spinners/RingLoader";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { Link } from "react-router-dom";
import GoogleIcon from "@mui/icons-material/Google";
import { css } from "@emotion/react";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../auth/authContext";
import { types } from "../../types/types";
import { GoogleLogin, GoogleLogout } from "react-google-login";

import "./Formbasic.scss";
const override = css`
  display: block;
  margin: 0 auto;
  border-color: "#fff";
`;

const theme = createTheme({
  palette: {
    primary: {
      main: "rgb(80, 43, 207)",
    },
  },
});

const Formbasic = ({ handleLogin, loading, error }) => {
  const navigate = useNavigate();
  const [loadingGoogle, setLoadingGoogle] = useState(false);

  var url = window.location.hostname.includes("localhost")
    ? "http://localhost:5000/api/auth/google"
    : "https://qpcode.herokuapp.com/api/auth/google";
  // var url = "https://qpcode.herokuapp.com/api/auth/google";
  const [form, setForm] = useState({ email: "", password: "" });
  const handleForm = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };
  const { dispatch } = useContext(AuthContext);

  const responseGoogle = (response) => {
    setLoadingGoogle(true);

    console.log(response);
    var id_token = response.tokenId;
    const data = { id_token };

    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    })
      .then((resp) => resp.json())
      .then((data) => (console.log("Nuestro server", data), GoogleData(data)))
      .catch(console.log);
  };

  const GoogleData = (data) => {
    const dataUsuario = data.usuario;
    setLoadingGoogle(false);

    const userQPCode = {
      idUser: dataUsuario.uid,
      names: dataUsuario.names,
      token: data.token,
    };
    const action = {
      type: types.loginUser,
      payload: { userQPCode },
    };
    dispatch(action);

    navigate("/perfil", {
      replace: true,
    });
  };

  return (
    <form
      className="formbasicLogin"
      onSubmit={(e) => {
        e.preventDefault();
        handleLogin(form);
      }}
    >
      <div className="formbasicLogin__title mb-1">Iniciar Sesión</div>
      <div className="formbasicLogin__subtitle mb-5">
        Ingresa tus credenciales
      </div>
      <ThemeProvider theme={theme}>
        <TextField
          id="login-email"
          label="Email"
          variant="outlined"
          name="email"
          fullWidth
          type="email"
          className="mb-5"
          value={form.email}
          placeholder="Ingresa tu correo electrónico"
          focused
          disabled={loading}
          onChange={handleForm}
          data-test-id="login-email-input"
        />
        <TextField
          id="login-password"
          label="Password"
          variant="outlined"
          name="password"
          fullWidth
          type="password"
          className="mb-5"
          value={form.password}
          placeholder="Ingresa tu contraseña"
          focused
          disabled={loading}
          onChange={handleForm}
          data-test-id="login-password-input"
        />
      </ThemeProvider>
      <div className="formbasicLogin__buttons  mb-4">
        <div className="formbasicLogin__buttons__loginNormal">
          {form.password === "" || form.email === "" ? (
            <button type="button" className="button" disabled>
              Ingresar
            </button>
          ) : (
            <button
              className="button"
              disabled={loading}
              data-test-id="login-btn-ingresar"
            >
              {loading ? (
                <RingLoader color={"#fff"} css={override} size={40} />
              ) : (
                "Ingresar"
              )}
            </button>
          )}
        </div>
        <div className="formbasicLogin__buttons__loginGoogle">
          <GoogleLogin
            clientId="655844817021-astk30osumrqo4iebkrjcp7ndkhov0ns.apps.googleusercontent.com"
            render={(renderProps) => (
              <button
                className="formbasicLogin__buttons__loginGoogle__button"
                onClick={renderProps.onClick}
                disabled={renderProps.disabled}
              >
                {loadingGoogle ? (
                  <RingLoader
                    color={"rgb(80, 43, 207)"}
                    css={override}
                    size={40}
                  />
                ) : (
                  <>
                    <GoogleIcon style={{ marginRight: "10px" }} />
                    Google
                  </>
                )}
              </button>
            )}
            onSuccess={responseGoogle}
            onFailure={responseGoogle}
            cookiePolicy={"single_host_origin"}
            fullWidth
          />
        </div>
      </div>

      <div className="formbasicLogin__texto">
        ¿No tienes una cuenta?
        <Link to="/register" data-test-id="login-page-button-register">
          <span className="formbasicLogin__texto--link">Registrate</span>
        </Link>
      </div>
      <div className="formbasicLogin__texto  mt-2">
        <Link to="/restore-password">
          <span
            className="formbasicLogin__texto--link"
            style={{ fontSize: 15, marginLeft: 0 }}
          >
            ¿Olvidaste tu contraseña?
          </span>
        </Link>
      </div>
    </form>
  );
};

export default Formbasic;
