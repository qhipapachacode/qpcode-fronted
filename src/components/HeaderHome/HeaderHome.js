import React, { useContext } from "react";
import SecondaryButton from "../SecondaryButoon/SecondaryButton";
import robot1 from "../../assets/svg/logo/robot1.svg";
import brazorobot from "../../assets/svg/logo/brazorobot.svg";
import drone from "../../assets/svg/logo/drone.svg";
import Particle from "../../components/Particle/Particle";
import { AuthContext } from "../../auth/authContext";
import { types } from "../../types/types";
import { Fade } from "react-reveal";
import Linksredes from "../LinksRedes/LinksRedes";
import "./HeaderHome.scss";
function HeaderHome() {
  const { user } = useContext(AuthContext);

  return (
    <div className="header">
      <div className="header__content">
        <Particle />

        <div className="headerStyled">
          <Fade left cascade>
            <div className="header__content__left">
              <div className="header__content__left__text">
                <h1 data-test-id="home-header-title">QhiPa-Co</h1>
                <div className="header__content__left__text__redes">
                  <Linksredes />
                </div>

                <p className="header__content__left__text__descripcion">
                  Si te gusta la programación, la innovación, el diseño o el
                  emprendimiento; asimismo, te gustaria brindar tus
                  conocimientos a estudiantes de diversas partes del Perú,
                  participar en concursos y mejorar tus habilidades, entonces
                  ¡Únete a esta gran familia! 🚀
                </p>
                <div className="header__content__left__text__button">
                  {user.userQPCode === undefined ? (
                    <SecondaryButton name={"Ingresa Aqui"} qpLink={"/login"} />
                  ) : (
                    <SecondaryButton name={"Ingresa Aqui"} qpLink={"/perfil"} />
                  )}
                </div>
              </div>
            </div>
          </Fade>
          <Fade right>
            <div className="header__content__right">
              <img
                src={robot1}
                alt=""
                className="header__content__right__robot"
              />
              <img
                src={drone}
                alt=""
                className="header__content__right__drone"
              />
              <img
                src={brazorobot}
                alt=""
                className="header__content__right__brazorobot"
              />
            </div>
          </Fade>
        </div>
      </div>
    </div>
  );
}

export default HeaderHome;
