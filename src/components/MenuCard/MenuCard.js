import React from "react";
import YouTubeIcon from "@mui/icons-material/YouTube";
import DoubleArrowIcon from "@mui/icons-material/DoubleArrow";
import { Fade, Flip } from "react-reveal";

import { Link } from "react-router-dom";

import "./MenuCard.scss";

const MenuCard = ({ menuItem }) => {
  console.log(menuItem);
  const filteredData = menuItem.filter((item) => item.outstanding === true);
  console.log(filteredData);

  return (
    <div className="menuCard" data-test-id="home-courses-cards-elements">
      {filteredData.map((item, i) => {
        return (
          <Fade key={item._id}>
            <div className="menuCard__grid">
              <div className="menuCard__grid__content">
                <div className="menuCard__grid__content__image">
                  <img src={item.img} alt="" />
                  <ul>
                    {item.LinkYoutube && (
                      <li>
                        <a
                          target="_blank"
                          href={item.LinkYoutube}
                          rel="noreferrer"
                        >
                          <YouTubeIcon />
                        </a>
                      </li>
                    )}
                  </ul>
                </div>
                <h6
                  className="menuCard__grid__content__title"
                  data-test-id={i === 0 ? "home-courses-card-title" : ""}
                >
                  {item.nameCourse}
                </h6>
                <p
                  className="menuCard__grid__content__subtitle"
                  data-test-id={i === 0 ? "home-courses-card-instructor" : ""}
                >
                  Instructor: {item.instructor.lastNames}
                </p>
                <div className="indications__button">
                  <Link
                    to={`/courses/${item.nameCourse
                      .toLowerCase()
                      .replaceAll(" ", "-")}`}
                  >
                    <button
                      type="button"
                      className="menuCard__grid__content__button"
                      data-test-id={
                        i === 0 ? "home-courses-card-redirect-button" : ""
                      }
                    >
                      <span>Ir al curso</span>
                      <DoubleArrowIcon style={{ marginLeft: "15x" }} />
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </Fade>
        );
      })}
    </div>
  );
};

export default MenuCard;
