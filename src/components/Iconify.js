import React from "react";
import PropTypes from "prop-types";
// icons
import { Icon } from "@iconify/react";
// @mui
import { Box } from "@mui/material";

// ----------------------------------------------------------------------\
import { disableCache } from "@iconify/react";

disableCache("all");

Iconify.propTypes = {
  icon: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  sx: PropTypes.object,
};

export default function Iconify({ icon, sx, ...other }) {
  // Iconify.disableCache("all");

  return <Box component={Icon} icon={icon} sx={{ ...sx }} {...other} />;
}
