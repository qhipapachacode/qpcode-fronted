export const types = {
  loginUser: "[auth] LoginUser",
  logoutUser: "[auth] LogoutUser",
  loginAdmin: "[auth] LoginAdmin",
  logoutAdmin: "[auth] LogoutAdmin",
};
