import styled from "styled-components";

export const OuterLayout = styled.section`
  padding: 5rem 18rem;
  @media screen and (max-width: 1347px) {
    padding: 5rem 14rem;
  }
  @media screen and (max-width: 1186px) {
    padding: 5rem 8rem;
  }
  @media screen and (max-width: 990px) {
    padding: 5rem 4rem;
  }
`;

export const InnerLayout = styled.section`
  padding: 8rem 0;
  @media screen and (max-width: 990px) {
    display: block;
  }
`;

export const MainLayout = styled.div`
  padding: 5rem;
  @media screen and (max-width: 642px) {
    padding: 4rem;
  }

  @media screen and (max-width: 571px) {
    padding: 2rem 0.4rem;
  }
`;

export const Adminlayout = styled.div`
  margin: 0 0 0 280px;
  @media screen and (max-width: 1199.5px) {
    margin: 0 0 0 0;
  }
`;
