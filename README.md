# QhipaPachaCode Frontend

### [Despliegue Web](https://qpcode.netlify.app/ "Despliegue")

### [API Despliegue](https://qpcode.herokuapp.com "API QPCode")

### [API Documentation](https://documenter.getpostman.com/view/17190637/UVsHV8mb "API Documentation")

QPCode es una plataforma Web para la comunidad "Qhipa-Co", grupo donde se toca temas de programación, innovación y diseño; asimismo, sus intengrantes participan en concursos, mejorarando sus habilidades y conocimientos 🚀

## Installation

Requires [Node.js](https://nodejs.org/) v16+ to run.

Install the dependencies and devDependencies

```
npm install
```

## Run

```
npm start
```

## Test:

```
npm run cypress
```

## License

MIT

## :+1: Sígueme en mis redes sociales:

- WebPersonal: (https://jorge-vicuna.gitlab.io/jorge-vicuna/)
- GitLab: (https://gitlab.com/jorge_vicuna)
- Youtube: (https://www.youtube.com/channel/UCW0m1TKKiN3Etejqx6h3Jtg)
- Linkedin: (https://www.linkedin.com/in/jorge-vicuna-valle/)
- Facebook: (https://www.facebook.com/jorge.vicunavalle/)

<table>
    <td align="center" >
      <a href="https://jorge-vicuna.gitlab.io/jorge-vicuna/">
        <img src="https://jorge-vicuna.gitlab.io/jorge-vicuna/static/media/avatar.272f0e79.jpg" width="100px;" alt=""/>
        <br />
        <sub><b>Jorge Vicuña Valle</b></sub>
      </a>
            <br />
      <span>♌🍗🎸🏀</span>
    </td>
</Table>
